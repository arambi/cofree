;(function(){
  $(function(){
        function loading( el){
          var width = el.width();
          var height = el.height();
          el.data( 'width', el.css( 'width'));
          el.data( 'height', el.css( 'height'));
          var before = $('<div>')
          ;
            
          el
            .addClass( 'is-loading')
            .data( 'html', el.html())
            .data( 'before', before)
            .html( '<i class="fa fa-spinner fa-spin fa-fw"></i>')
            .width( width)
            .height( height)
            .before( before)
          ;
    
          $(before)
            .copyCSS( el, null, ['top', 'left', 'position', 'z-index'])
          ;
    
          $(before).css({
              position: 'absolute',
              left: el.position().left,
              top: (el.position().top - ($(before).outerHeight(true))) + 'px',
              'z-index': '99999999999',
              'background-color': 'transparent',
              'border': 'none'
            })
        }
    
        $('body').delegate( '[spinner-btn]', 'loading', function(){
          loading( $(this));
        });    
        
        
        $('body').delegate( '[spinner-btn]', 'stoping', function(){
          $(this)
            .removeClass( 'is-loading')
            .css({
              width: $(this).data( 'width'), 
              height: $(this).data( 'height') 
            })
            .html( $(this).data( 'html'))
          ;
          $($(this).data( 'before')).remove();
        });
      });
})()