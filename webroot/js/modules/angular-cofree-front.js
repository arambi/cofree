angular.module( 'cofreeFront', [])

  .directive( 'rawdata', function( $timeout, $rootScope, $parse) {
    return {
      restrict: 'A',
      scope: '=',
      link: function(scope, element, attrs) { 
          var data = (element[0].innerHTML);
          var variable = $parse( attrs.rawdata);
          variable.assign( $rootScope, JSON.parse(data))
          $timeout( function(){
            $rootScope.$apply();
          })
      }
    }
  });

;