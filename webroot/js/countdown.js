/*
* Basic Count Down to Date and Time
* Author: @mrwigster / trulycode.com
*/
(function (e) {
      e.fn.countdown = function (t, n) {
          function i() {
                eventDate = Date.parse(r.date) / 1e3;
                currentDate = Math.floor(e.now() / 1e3);

                if (eventDate <= currentDate) {
                  n.call(this);
                  clearInterval(interval);
                }

                seconds = eventDate - currentDate;
                days = Math.floor(seconds / 86400);
                seconds -= days * 60 * 60 * 24;
                hours = Math.floor(seconds / 3600);
                seconds -= hours * 60 * 60;
                minutes = Math.floor(seconds / 60);
                seconds -= minutes * 60;

                days == 1 ? thisEl.find(".timeRefDays").text("Tag") : thisEl.find(".timeRefDays").text("Tage");
                hours == 1 ? thisEl.find(".timeRefHours").text("Stunde") : thisEl.find(".timeRefHours").text("Stunden");
                minutes == 1 ? thisEl.find(".timeRefMinutes").text("Minute") : thisEl.find(".timeRefMinutes").text("Minuten");
                seconds == 1 ? thisEl.find(".timeRefSeconds").text("Sekunde") : thisEl.find(".timeRefSeconds").text("Sekunden");

                if (hours == 0) {
                    thisEl.find('.hours, .dp_h').hide();
                    thisEl.find(".timeRefHours").text("Minuten");
                };
                if (hours == 0 && minutes == 0) {
                    thisEl.find('.hours, .dp_h').hide();
                    thisEl.find('.minutes, .dp_m').hide();
                    thisEl.find(".seconds").css("font", "900 15px lato");
                    thisEl.find(".timeRefHours").text("Sekunden");
                }
//                if (hours > 0) {
//                    thisEl.find(".seconds, .dp_m").hide();
//                }

                if (r["format"] == "on") {
                  //days = String(days).length >= 2 ? days : "0" + days;
                  hours = String(hours).length >= 2 ? hours : "0" + hours;
                  minutes = String(minutes).length >= 2 ? minutes : "0" + minutes;
                  seconds = String(seconds).length >= 2 ? seconds : "0" + seconds;
                };


                if (days == 0 && hours >= 11) {
                    thisEl.find(".hours").text('00');
                    thisEl.find(".minutes").text('00');
                    thisEl.find(".seconds").text('00');
                    return;
                };

                if (!isNaN(eventDate)) {
                  thisEl.find(".hours").text(hours);
                  thisEl.find(".minutes").text(minutes);
                  thisEl.find(".seconds").text(seconds);
                } else {
                    alert("Invalid date. Example: 30 Tuesday 2013 15:50:00");
                    clearInterval(interval)
                }
          }
          var thisEl = e(this);
          var r = {
                  date: null,
                  format: null
          };
          t && e.extend(r, t);
          i();
          interval = setInterval(i, 1e3)
      };

})(jQuery);

$(function(){
  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
  };

  function unformatNumber(num) {
      return num.toString().replace(".", "")
  };
  
  $('.counter-number').each( function(){
      var counter = $(this);
      var number = counter.attr("data-count");
      var attr = counter.attr("data-attr");
      var text = 0;
      setInterval(function(){
          var bottom_of_object = counter.offset().top;
          var bottom_of_window = $(window).scrollTop() + $(window).height();
          if( bottom_of_window > (bottom_of_object + 150) ){
              text = text + (number / 50);
              if (text < number){
                  counter.text(formatNumber(text.toFixed(0)) + attr);
              }
              else{
                  counter.text(formatNumber(number) + attr);
              };
          };
      }, 100);
  });


})
