<?php
namespace Cofree\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Cofree\View\Helper\InjectCSSHelper;

/**
 * Cofree\View\Helper\InjectCSSHelper Test Case
 */
class InjectCSSHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Cofree\View\Helper\InjectCSSHelper
     */
    public $InjectCSS;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->InjectCSS = new InjectCSSHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InjectCSS);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
