<?php
namespace Cofree\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Cofree\View\Helper\BrowserHelper;

/**
 * Cofree\View\Helper\BrowserHelper Test Case
 */
class BrowserHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Cofree\View\Helper\BrowserHelper
     */
    public $Browser;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Browser = new BrowserHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Browser);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
