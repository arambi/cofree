<?php
namespace Cofree\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Cofree\View\Helper\UtilHelper;

/**
 * Cofree\View\Helper\UtilHelper Test Case
 */
class UtilHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Cofree\View\Helper\UtilHelper
     */
    public $Util;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Util = new UtilHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Util);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
