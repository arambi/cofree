<?php
namespace Cofree\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Cofree\View\Helper\SocialnetHelper;

/**
 * Cofree\View\Helper\SocialnetHelper Test Case
 */
class SocialnetHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Cofree\View\Helper\SocialnetHelper
     */
    public $Socialnet;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Socialnet = new SocialnetHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Socialnet);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
