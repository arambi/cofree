<?php
namespace Cofree\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Cofree\View\Helper\WeatherHelper;

/**
 * Cofree\View\Helper\WeatherHelper Test Case
 */
class WeatherHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Cofree\View\Helper\WeatherHelper
     */
    public $Weather;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Weather = new WeatherHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Weather);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
