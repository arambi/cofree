<?php
namespace Cofree\Test\TestCase\Shell;

use Cake\TestSuite\TestCase;
use Cofree\Shell\ThemeShell;

/**
 * Cofree\Shell\ThemeShell Test Case
 */
class ThemeShellTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->io = $this->getMock('Cake\Console\ConsoleIo');
        $this->Theme = new ThemeShell($this->io);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Theme);

        parent::tearDown();
    }

    /**
     * Test main method
     *
     * @return void
     */
    public function testMain()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
