<?php 

namespace Cofree\Test\TestCase\Lib;

use Cake\TestSuite\TestCase;
use Cofree\Lib\RemoteLocation;

class RemoteLocationTest extends TestCase
{	
	public function setUp() 
	{
		parent::setUp();
	}

	public function tearDown() 
  {
    parent::tearDown();
  }

	public function testConstruct()
	{
		$_SERVER ["SERVER_ADDR"] = '46.39.53.108';
		$location = new RemoteLocation();

		$this->assertInstanceOf( 'Cofree\Lib\RemoteLocation', $location);
		$this->assertEquals( 'RU', $location->country_code);
		$this->assertEquals( 'RUS', $location->country_code3);
	}
}