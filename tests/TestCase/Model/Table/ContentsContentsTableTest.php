<?php
namespace Cofree\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Cofree\Model\Table\ContentsContentsTable;

/**
 * Cofree\Model\Table\ContentsContentsTable Test Case
 */
class ContentsContentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Cofree\Model\Table\ContentsContentsTable
     */
    public $ContentsContents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Cofree.ContentsContents',
        'plugin.Cofree.Contents',
        'plugin.Cofree.Foreigns',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ContentsContents') ? [] : ['className' => ContentsContentsTable::class];
        $this->ContentsContents = TableRegistry::getTableLocator()->get('ContentsContents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContentsContents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
