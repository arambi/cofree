<?php
namespace Cofree\Test\TestCase\Model\Behavior;

use Cofree\Model\Behavior\ContentableBehavior;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\TestSuite\TestCase;

class ContentsTable extends Table 
{

	public function initialize(array $options) 
  {
		$this->addBehavior( 'Cofree.Contentable');
	}
}

/**
 * Cofree\Model\Behavior\ContentableBehavior Test Case
 */
class ContentableBehaviorTest extends TestCase 
{

	public $fixtures = [
    'plugin.cofree.contents',
  ];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);	
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Contentable);

		parent::tearDown();
	}

/**
 * Comprueba que se guarda correctamente
 */
  public function testSave()
  {
    $content = $this->Contents->newEntity([
      'title' => 'Hello word'
    ]);

    if( $this->Contents->save( $content))
    {
      $this->assertEquals( 'Contents', $content->content_type);
    }
    else
    {
      $this->assertTrue( false);
    }
  }

/**
 * Comprueba que se lee correctamente
 */
  public function testRead()
  {
    $count = $this->Contents->find()->count();
    $this->assertEquals( 1, $count);
  }




}
