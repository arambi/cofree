<?php
namespace Cofree\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use Cofree\Model\Behavior\SortableBehavior;

/**
 * Cofree\Model\Behavior\SortableBehavior Test Case
 */
class SortableBehaviorTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Cofree\Model\Behavior\SortableBehavior
     */
    public $Sortable;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Sortable = new SortableBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sortable);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
