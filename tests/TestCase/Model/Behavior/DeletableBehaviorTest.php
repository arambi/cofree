<?php
namespace Cofree\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use Cofree\Model\Behavior\DeletableBehavior;

/**
 * Cofree\Model\Behavior\DeletableBehavior Test Case
 */
class DeletableBehaviorTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Cofree\Model\Behavior\DeletableBehavior
     */
    public $Deletable;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Deletable = new DeletableBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Deletable);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
