<?php
namespace Cofree\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use Cofree\Model\Behavior\SaltableBehavior;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;

class ContentsTable extends Table 
{
  public function initialize( array $options) 
  {
    $this->addBehavior( 'Cofree.Saltable');
  }
}


/**
 * Cofree\Model\Behavior\SaltableBehavior Test Case
 */
class SaltableBehaviorTest extends TestCase
{

  public $fixtures = [
    'plugin.cofree.contents',
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp() 
  {
    parent::setUp();
    $this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]); 
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    unset( $this->Saltable);
    unset( $this->Contents);

    parent::tearDown();
  }

  public function testSave()
  {
    $content = $this->Contents->newEntity([
      'title' => 'Hello word'
    ]);

    if( $this->Contents->save( $content))
    {
      $this->assertTrue( $content->has( 'salt'));
      $salt = $content->salt;
      $this->assertTrue( !empty( $salt));
    }
    else
    {
      $this->assertTrue( false);
    }
  }

    
}
