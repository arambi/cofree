<?php
namespace Cofree\Test\TestCase\Model\Behavior;

use Cofree\Model\Behavior\JsonableBehavior;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\TestSuite\TestCase;

class ContentsTable extends Table 
{

	public function initialize(array $options) 
  {
		$this->addBehavior( 'Cofree.Jsonable', [
			'fields' => [
				'body'
			]
		]);
	}

	
}


/**
 * Cofree\Model\Behavior\JsonableBehavior Test Case
 */
class JsonableBehaviorTest extends TestCase 
{
	public $fixtures = [
    'plugin.cofree.contents',
  ];
  
/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);	
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Contents);

		parent::tearDown();
	}


	public function testSave()
	{
		$content = $this->Contents->newEntity([
			'title' => 'Hello',
			'body' => [
				'firstname' => 'Gorka',
				'lastname' => 'Garro',
				'coords' => [
						'city' => 'Pamplona',
						'state' => 'Navarra'
				]
			]
		]);

		$this->Contents->save( $content);

		$body = json_decode( $content->body);

		$this->assertTrue( is_object( $body));
		$this->assertEquals( $body->firstname, 'Gorka');

		$content = $this->Contents->find()->where(['title' => 'Hello'])->first();
		$this->assertEquals( $content->body->coords->city, 'Pamplona');
	}

}
