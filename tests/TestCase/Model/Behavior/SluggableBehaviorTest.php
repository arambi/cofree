<?php
namespace Cofree\Test\TestCase\Model\Behavior;

use Cofree\Model\Behavior\SluggableBehavior;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\TestSuite\TestCase;
use Cake\I18n\I18n;
use Cake\Core\Plugin;
use Cake\ORM\TableRegistry;


class ContentsTable extends Table 
{

	public function initialize(array $options) 
  {
		
	}
}

/**
 * Cofree\Model\Behavior\SluggableBehavior Test Case
 */
class SluggableBehaviorTest extends TestCase 
{

  public $fixtures = [
    'plugin.cofree.contents',
    'plugin.i18n.languages',
    'plugin.manager.translates',
  ];
/**
 * setUp method
 *
 * @return void
 */
	public function setUp() 
  {
    // Quita el plugin section si está leído
    if( Plugin::loaded( 'Section'))
    {
      Plugin::unload( 'Section');
    }

    // Quita el plugin Website si está leído
    if( Plugin::loaded( 'Website'))
    {
      Plugin::unload( 'Website');
    }

		parent::setUp();
		$this->connection = ConnectionManager::get( 'test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);

    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
  }

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() 
  {
		unset( $this->connection);
    unset( $this->Contents);
    unset( $this->Languages);
		parent::tearDown();
	}

 

/**
 * Comprueba que se guarda correctamente
 */
  public function testSave()
  {
    $this->Contents->addBehavior( 'Cofree.Sluggable');
    $content = $this->Contents->newEntity([
      'title' => 'Hello word'
    ]);

    $this->Contents->save( $content);
    $this->assertEquals( 'hello-word', $content->slug);
  }

  /**
 * Comprueba que se guarda correctamente
 */
  public function testSaveManyFields()
  {
    $this->Contents->addBehavior( 'Cofree.Sluggable', [
      'field' => [
        'title', 'subtitle'
      ]
    ]);

    $content = $this->Contents->newEntity([
      'title' => 'Hello word',
      'subtitle' => 'Hola mundo',
    ]);

    $this->Contents->save( $content);
    $this->assertEquals( 'hello-word-hola-mundo', $content->slug);
  }

}
