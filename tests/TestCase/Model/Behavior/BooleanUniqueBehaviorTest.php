<?php
namespace Cofree\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use Cofree\Model\Behavior\BooleanUniqueBehavior;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;


class ContentsTable extends Table 
{

  public function initialize(array $options) 
  {
    $this->addBehavior( 'Cofree.BooleanUnique', [
        'fields' => [
            'published'
        ]
    ]);
  }
}

/**
 * Cofree\Model\Behavior\BooleanUniqueBehavior Test Case
 */
class BooleanUniqueBehaviorTest extends TestCase
{

  public $fixtures = [
    'plugin.cofree.contents',
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]); 
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    unset( $this->Contents);
    parent::tearDown();
  }

  public function testSave()
  {
    $content = $this->Contents->get( 1);
    $data = [
      'published' => 1
    ];

    $content = $this->Contents->patchEntity( $content, $data);
    $this->Contents->save( $content);
    $query = $this->Contents->find()->where( ['published' => 1]);
    $count = $query->count();
    $this->assertEquals( $count, 1);
    $unique = $query->first();
    $this->assertEquals( $content->id, $unique->id);
  }
}
