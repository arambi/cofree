<?php
namespace Cofree\Test\TestCase\Model\Behavior;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\TestSuite\TestCase;

class ContentsTable extends Table 
{

  public function initialize(array $options) 
  {
    $this->addBehavior( 'Cofree.Publisher');
  }
}

/**
 * Cofree\Model\Behavior\PublisherBehavior Test Case
 */
class PublisherBehaviorTest extends TestCase
{
  public $fixtures = [
    'plugin.cofree.contents',
  ];
  
  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]); 
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
      unset($this->Publisher);

      parent::tearDown();
  }

  /**
   * Test initial setup
   *
   * @return void
   */
  public function testFromToQuery()
  {
    $content = $this->Contents->find()->first();

    $start_on = date( 'Y-m-d', strtotime( '-2 day', time()));
    $finish_on = date( 'Y-m-d', strtotime( '+2 day', time()));

    $content->start_on = $start_on;
    $content->finish_on = $finish_on;
    $content->date_restrict = 1;

    $this->Contents->save( $content);

    $query = $this->Contents->find()->where(['id' => $content->id]);

    $this->Contents->fromToQuery( $query);

    $new = $query->first();

    $this->assertEquals( $content->id, $new->id);

    // 
    
    $start_on = date( 'Y-m-d', strtotime( '+3 day', time()));
    $finish_on = date( 'Y-m-d', strtotime( '+5 day', time()));

    $content->start_on = $start_on;
    $content->finish_on = $finish_on;
    $content->date_restrict = 1;

    $this->Contents->save( $content);

    $query = $this->Contents->find()->where(['id' => $content->id]);

    $this->Contents->fromToQuery( $query);

    $new = $query->first();
    $this->assertTrue( !$new);


    // 
    
    $start_on = date( 'Y-m-d', strtotime( '+3 day', time()));
    $finish_on = date( 'Y-m-d', strtotime( '+5 day', time()));

    $content->start_on = $start_on;
    $content->finish_on = $finish_on;
    $content->date_restrict = 0;

    $this->Contents->save( $content);

    $query = $this->Contents->find()->where(['id' => $content->id]);

    $this->Contents->fromToQuery( $query);

    $new = $query->first();
    $this->assertEquals( $content->id, $new->id);
    
  }
}
