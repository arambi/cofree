$('body').delegate('[tab]', 'click', function(){
  var isOpen = $($(this).attr( 'tab-id')).hasClass( 'active');
  $('[tab=' + $(this).attr( 'tab')  + ']').removeClass( 'active');
  $('[tab-content=' + $(this).attr( 'tab')  + ']').removeClass( 'active');

  if( isOpen){
    $($(this).attr( 'tab-id')).removeClass( 'active');
    $(this).removeClass( 'active');
  } else {
    $($(this).attr( 'tab-id')).addClass( 'active');
    $(this).addClass( 'active');
  }
});

