# Cofree Plugin

Diversas utilidades de ayuda a hacer un código más limpio y eficaz.

## BooleanUniqueBehavior

Cuando se guarda en una columna booleana un valor 1, se asignarán a todos los registros del model el valor 0. Este Behavior es utilizado para aquellos casos en lo que queremos tener un valor que solo puede darse en un registro. Por ejemplo, la página homepage de las secciones, solo puede haber una y cuando se marca como tal, el registro que tuviera el valor 1, debe ser automáticamente marcado a 0.

### Uso

Para enchufar el Behavior tan solo es necesario indicarle en la configuración un array con un key `fields` que tenga como valor un array con los campos que queremos que sean de valor booleano único.

```php
 $this->addBehavior( 'Cofree.BooleanUniqueBehavior', ['fields' => ['is_homepage']]);
```

