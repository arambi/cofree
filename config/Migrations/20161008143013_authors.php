<?php

use Phinx\Migration\AbstractMigration;

class Authors extends AbstractMigration
{

  public function change()
  {
    $contents = $this->table( 'contents');
    $contents
          ->addColumn( 'author_id', 'integer', ['null' => true, 'default' => null])
          ->addIndex( ['author_id'])
          ->save();      
  }
}
