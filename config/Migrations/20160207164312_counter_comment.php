<?php

use Phinx\Migration\AbstractMigration;

class CounterComment extends AbstractMigration
{

  public function up()
  {
    $contents = $this->table( 'contents');
    $contents
      ->addColumn( 'comment_count', 'integer', ['null' => false, 'default' => 0])
      ->addIndex( ['comment_count'])
      ->update();
  }
  
  public function down()
  {
    $contents = $this->table( 'contents');
    $contents->removeColumn( 'comment_count')->update();
    $contents->removeIndex( 'comment_count')->update();
  }
}
