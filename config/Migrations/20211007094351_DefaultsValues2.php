<?php

use Migrations\AbstractMigration;

class DefaultsValues2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('rows')
            ->changeColumn('position', 'integer', ['limit' => 6, 'null' => true, 'default' => null])
            ->update();

        $this->table('columns')
            ->changeColumn('position', 'integer', ['limit' => 6, 'null' => true, 'default' => null])
            ->update();

        $this->table('sections')
            ->changeColumn('position', 'integer', ['limit' => 6, 'null' => true, 'default' => null])
            ->update();

        $this->table('links')
            ->changeColumn('position', 'integer', ['limit' => 6, 'null' => true, 'default' => null])
            ->update();
    }
}
