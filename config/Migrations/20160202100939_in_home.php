<?php

use Phinx\Migration\AbstractMigration;

class InHome extends AbstractMigration
{
  public function up()
  {
    $contents = $this->table( 'contents');
    $contents
      ->addColumn( 'special', 'boolean', ['null' => true, 'default' => 0])
      ->addIndex( ['special'])
      ->update();
  }
  
  public function down()
  {
    $contents = $this->table( 'contents');
    $contents->removeColumn( 'special')->update();
    $contents->removeIndex( 'special')->update();
  }
}
