<?php

use Phinx\Migration\AbstractMigration;

class ViewCount extends AbstractMigration
{
  public function change()
  {
    $contents = $this->table( 'contents');
    $contents
          ->addColumn( 'view_count', 'integer', ['null' => false, 'default' => 0])
          ->addIndex( ['view_count'])
          ->save();      
  }
}
