<?php
use Migrations\AbstractMigration;

class TranslateUrl extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $contents = $this->table( 'contents_translations');

    if( !$contents->hasColumn( 'url'))
    {
      $contents
        ->addColumn( 'url', 'string', ['null' => true, 'default' => null, 'limit' => 255])
        ->update();  
    }
  }
}
