<?php

use Phinx\Migration\AbstractMigration;

class EventDateFormat extends AbstractMigration
{

  public function change()
  {
    $contents = $this->table( 'contents');
    $contents
      ->addColumn( 'dont_show_dates', 'boolean', ['default' => 0, 'null' => false])
      ->addColumn( 'has_date_string', 'boolean', ['default' => 0, 'null' => false])
      ->addColumn( 'date_string', 'string', ['default' => null, 'null' => true])
      ->save();
  }
}
