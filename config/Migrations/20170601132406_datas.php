<?php

use Phinx\Migration\AbstractMigration;

class Datas extends AbstractMigration
{
   
  public function change()
  {
    $contents = $this->table( 'contents');
    $contents
      ->addColumn( 'data1', 'string', ['default' => null, 'null' => true])
      ->addColumn( 'data2', 'string', ['default' => null, 'null' => true])
      ->addColumn( 'data3', 'string', ['default' => null, 'null' => true])
      ->addColumn( 'data4', 'string', ['default' => null, 'null' => true])
      ->addColumn( 'data5', 'string', ['default' => null, 'null' => true])
      ->save();
  }
}
