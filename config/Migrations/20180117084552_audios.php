<?php
use Migrations\AbstractMigration;

class Audios extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $contents = $this->table( 'contents');

    if( !$contents->hasColumn( 'audios'))
    {
      $contents->addColumn( 'audios', 'text', ['default' => null, 'null' => true]);
      $contents->update();
    }

  
  }
}
