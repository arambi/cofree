<?php

use Phinx\Migration\AbstractMigration;

class ContentTranslations extends AbstractMigration
{

  public function up()
  {
    $contents = $this->table( 'contents_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $contents
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'antetitle', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'subtitle', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'date_string', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'summary', 'text', ['default' => NULL, 'null' => true])
      ->addColumn( 'summary2', 'text', ['default' => NULL, 'null' => true])
      ->addColumn( 'body', 'text', ['default' => NULL, 'null' => true])
      ->save();  
  }

  public function down()
  {
    $this->dropTable( 'contents_translations');
  }
}
