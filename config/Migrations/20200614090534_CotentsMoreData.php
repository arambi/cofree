<?php
use Migrations\AbstractMigration;

class CotentsMoreData extends AbstractMigration
{
	/**
	 * Change Method.
	 *
	 * More information on this method is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-change-method
	 * @return void
	 */
	public function change()
	{
		$contents = $this->table( 'contents');
		$columns = ['data6', 'data7', 'data8', 'data9'];

		foreach( $columns as $column) {
			if( !$contents->hasColumn( $column)) {
				$contents
					->addColumn( $column, 'string', ['null' => true, 'default' => null])
					->update();
			}    
		}
	}
}
