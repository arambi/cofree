<?php

use Phinx\Migration\AbstractMigration;

class ContentsMap extends AbstractMigration
{

  public function up()
  {
    $contents = $this->table( 'contents');

    if( !$contents->hasColumn( 'lat'))
    {
      $contents->addColumn( 'lat', 'float', ['default' => null, 'null' => true]);
    }

    if( !$contents->hasColumn( 'lng'))
    {
      $contents->addColumn( 'lng', 'float', ['default' => null, 'null' => true]);
    }

    if( !$contents->hasColumn( 'zoom'))
    {
      $contents->addColumn( 'zoom', 'integer', ['default' => null, 'null' => true]);
    }
      
    $contents->save();
  }

  public function down()
  {
    $contents = $this->table( 'contents');

    if( $contents->hasColumn( 'lat'))
    {
      $contents->removeColumn( 'lat');
    }

    if( $contents->hasColumn( 'lng'))
    {
      $contents->removeColumn( 'lng');
    }

    if( $contents->hasColumn( 'zoom'))
    {
      $contents->removeColumn( 'zoom');
    }
      
    $contents->save();
  }
}
