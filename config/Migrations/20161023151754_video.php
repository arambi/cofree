<?php

use Phinx\Migration\AbstractMigration;

class Video extends AbstractMigration
{
  public function change()
  {
    $contents = $this->table( 'contents');
    $contents
          ->addColumn( 'video', 'string', ['null' => true, 'default' => null])
          ->save();      
  }
}
