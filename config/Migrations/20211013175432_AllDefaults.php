<?php

use Phinx\Db\Table;
use Cake\Database\Connection;
use Migrations\AbstractMigration;
use Cake\Datasource\ConnectionManager;

class AllDefaults extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $names = ConnectionManager::get('default')->getSchemaCollection()->listTables();

        foreach ($names as $name) {
            $table = $this->table($name);
            $this->proccessTable($table);
        }
    }

    private function proccessTable(Table $table)
    {
        $columns = $table->getColumns();
        
        print_r($table->getName());

        if (strpos($table->getName(), 'phinxlo') !== false) {
            return;
        }

        foreach ($columns as $column) {
            if ($column->getName() != 'id') {
                if (!$column->getNull() && is_null($column->getDefault())) {
                    $column->setNull(true);
                    $column->setDefault(null);
                    $table->changeColumn($column->getName(), $column);
                }
            }
        }

        $table->update();
    }
}
