<?php
use Migrations\AbstractMigration;

class ContentsHour2 extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $contents = $this->table( 'contents');
    
    if( !$contents->hasColumn( 'hour'))
    {
      $contents
        ->addColumn( 'hour', 'string', ['limit' => 5, 'null' => true, 'default' => null])
        ->addIndex( 'hour')
        ->addIndex( ['published_at', 'hour'])
        ->update();
    }    
  }
}
