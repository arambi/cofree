<?php

use Phinx\Migration\AbstractMigration;

class Draft extends AbstractMigration
{

  public function up()
  {
    $contents = $this->table( 'contents');
    $contents
      ->addColumn( 'is_draft', 'boolean', ['null' => true, 'default' => 0])
      ->update();
  }
  
  public function down()
  {
    $contents = $this->table( 'contents');
    $contents->removeColumn( 'draft')->update();
  }
}
