<?php
use Migrations\AbstractMigration;

class ContentsRating extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $contents = $this->table( 'contents');

        if (!$contents->hasColumn('rating_avg')) {
            $contents
                ->addColumn( 'rating_avg', 'float', ['precision' => 8, 'scale' => 4, 'null' => true, 'default' => null, 'limit' => 64])
                ->update();
        };
    }
}
