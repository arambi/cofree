<?php
use Migrations\AbstractMigration;

class ContentsSpecials2 extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
	{
		$contents = $this->table( 'contents');
		$columns = ['special4', 'special5'];

		foreach( $columns as $column) {
			if( !$contents->hasColumn( $column)) {
				$contents
					->addColumn( $column, 'boolean', ['null' => false, 'default' => 0])
					->update();
			}    
    }
    
		$contents = $this->table( 'contents');
		$columns = ['data10', 'data11', 'data12', 'data13'];

		foreach( $columns as $column) {
			if( !$contents->hasColumn( $column)) {
				$contents
					->addColumn( $column, 'string', ['null' => true, 'default' => null])
					->update();
			}    
		}
	}
}
