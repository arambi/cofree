<?php

use Phinx\Migration\AbstractMigration;

class NewFields extends AbstractMigration
{
  public function up()
  {
    $contents = $this->table( 'contents');
    $contents
      ->addColumn( 'summary2', 'text', ['default' => NULL, 'null' => true])
      ->addColumn( 'summary3', 'text', ['default' => NULL, 'null' => true])
      ->addColumn( 'body2', 'text', ['default' => NULL, 'null' => true])
      ->addColumn( 'body3', 'text', ['default' => NULL, 'null' => true])
      ->update();
  }
  
  public function down()
  {
    $contents = $this->table( 'contents');
    $contents->removeColumn( 'summary2')->update();
    $contents->removeColumn( 'summary3')->update();
    $contents->removeColumn( 'body2')->update();
    $contents->removeColumn( 'body3')->update();
  }
}
