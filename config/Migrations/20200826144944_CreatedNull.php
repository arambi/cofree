<?php
use Migrations\AbstractMigration;

class CreatedNull extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table( 'contents_contents')
            ->changeColumn( 'created', 'datetime', ['default' => null, 'null' => true])
            ->update();
    }
}
