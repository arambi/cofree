<?php
use Migrations\AbstractMigration;

class ContentsContentType extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $contents = $this->table( 'contents');
    $contents
      ->changeColumn( 'content_type', 'string', ['limit' => 24])
      ->update();
  }
}