<?php

use Phinx\Migration\AbstractMigration;

class ContentsSpecials extends AbstractMigration
{

  public function change()
  {
    $contents = $this->table( 'contents');
    $contents
      ->addColumn( 'special2', 'boolean', ['null' => true, 'default' => 0])
      ->addColumn( 'special3', 'boolean', ['null' => true, 'default' => 0])
      ->save();
  }
}
