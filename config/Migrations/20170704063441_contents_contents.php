<?php

use Phinx\Migration\AbstractMigration;

class ContentsContents extends AbstractMigration
{
    
  public function change()
  {
    // Tabla para las relaciones entre contents y categories
    if( !$this->hasTable( 'contents_contents'))
    {
      $contents_contents = $this->table( 'contents_contents');
      $contents_contents
            ->addColumn( 'content_id', 'integer', ['null' => false])
            ->addColumn( 'foreign_id', 'integer', ['null' => false])
            ->addColumn( 'created', 'datetime', ['default' => null])
            ->addIndex( ['content_id', 'foreign_id'])
            ->addIndex( ['content_id'])
            ->addIndex( ['foreign_id'])
            ->create();
    }
  }
}
