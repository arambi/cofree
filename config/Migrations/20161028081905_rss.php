<?php

use Phinx\Migration\AbstractMigration;

class Rss extends AbstractMigration
{
  public function change()
  {
    $contents = $this->table( 'contents');
    $contents
      ->addColumn( 'include_rss', 'boolean', ['default' => 1, 'null' => false])
      ->save();
  }
}
