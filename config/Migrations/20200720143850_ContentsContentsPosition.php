<?php
use Migrations\AbstractMigration;

class ContentsContentsPosition extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table( 'contents_contents')
            ->addColumn( 'position', 'integer', ['null' => true, 'default' => null, 'limit' => 3])
            ->addIndex( 'position')
            ->update();
    }
}
