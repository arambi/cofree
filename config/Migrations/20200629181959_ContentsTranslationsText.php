<?php
use Migrations\AbstractMigration;

class ContentsTranslationsText extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $contents = $this->table( 'contents_translations');

    $fields = ['summary2', 'summary3', 'body2', 'body3'];

    foreach( $fields as $field) {
      if( !$contents->hasColumn( $field)) {
        $contents
          ->addColumn( $field, 'text', ['null' => true, 'default' => null])
          ->update();  
      }
    }
  }
}
