<?php
/**
 * Cofree Migrations
 *
 * Contiene las tablas esenciales para cualquier tipo de contenido y sus taxonomías
 * 
 */
use Phinx\Migration\AbstractMigration;

class Initial extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
     *  
    public function change()
    {
      $this->table( 'contents')->addColumn( 'url', 'string', ['null' => true, 'default' => null])->update();
    }
   */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
      // Tabla para los models de contenido
      $contents = $this->table( 'contents');
      $contents
            // El tipo de contenido, el nombre del model
            ->addColumn( 'content_type', 'string', ['limit' => 16])
            ->addColumn( 'site_id', 'integer', ['null' => true, 'default' => null, 'limit' => 4])

            // Usado para relacionar contents con contents 
            ->addColumn( 'parent_id', 'integer', ['null' => true, 'default' => null])

            // Tipología
            ->addColumn( 'subtype', 'string', ['limit' => 32])

            // Usado para relacionar con otras tablas
            ->addColumn( 'category_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn( 'published', 'boolean', ['null' => true, 'default' => 0])
            ->addColumn( 'published_at', 'date', ['null' => true, 'default' => null])

            // Campos de texto
            ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
            ->addColumn( 'antetitle', 'string', ['null' => true, 'default' => null, 'limit' => 255])
            ->addColumn( 'subtitle', 'string', ['null' => true, 'default' => null, 'limit' => 255])
            ->addColumn( 'summary', 'text', ['default' => NULL, 'null' => true])
            ->addColumn( 'body', 'text', ['default' => NULL, 'null' => true])
            ->addColumn( 'url', 'string', ['null' => true, 'default' => null, 'limit' => 255])

            // Datos para los bloques
            ->addColumn( 'row', 'integer', ['null' => true, 'default' => null, 'limit' => 3])
            ->addColumn( 'cols', 'integer', ['null' => true, 'default' => null, 'limit' => 2])

            // Posición en el orden de elementos
            ->addColumn( 'position', 'integer', ['null' => true, 'default' => null, 'limit' => 6])

            // Json, donde se guardarán preferencias
            ->addColumn( 'settings', 'text', ['default' => NULL, 'null' => true])

            // Para cuestiones de seguridad
            ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])

            // Photo en Json
            ->addColumn( 'photo', 'text', ['default' => NULL, 'null' => true])
            ->addColumn( 'photos', 'text', ['default' => NULL, 'null' => true])
            ->addColumn( 'docs', 'text', ['default' => NULL, 'null' => true])


            ->addColumn( 'created', 'datetime', ['default' => null])
            ->addColumn( 'modified', 'datetime', ['default' => null])
            ->addIndex( ['content_type'])
            ->addIndex( ['site_id'])
            ->addIndex( ['category_id'])
            // ->addIndex( ['slug'])
            ->addIndex( ['parent_id'])
            ->addIndex( ['published_at'])
            ->addIndex( ['row'])
            ->addIndex( ['position'])
            ->save();      
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
      $this->dropTable( 'contents');
    }
}