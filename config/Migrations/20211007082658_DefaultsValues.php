<?php

use Migrations\AbstractMigration;

class DefaultsValues extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('contents')
            ->changeColumn('subtype', 'string', ['limit' => 32, 'default' => null, 'null' => true])
            ->update();

        if ($this->hasTable('mailchimp_campaigns')) {
            $this->table('mailchimp_campaigns')
                ->changeColumn('schedule_time', 'datetime', ['default' => null, 'null' => true])
                ->update();
        }

        $this->table('uploads')
            ->changeColumn('alt_content_type', 'text', ['default' => null, 'null' => true])
            ->update();
    }
}
