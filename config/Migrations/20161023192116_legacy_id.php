<?php

use Phinx\Migration\AbstractMigration;

class LegacyId extends AbstractMigration
{

  public function change()
  {
    $contents = $this->table( 'contents');
   
    if( !$contents->hasColumn( 'legacy_id'))
    {
      $contents
        ->addColumn( 'legacy_id', 'integer', ['null' => true, 'default' => null])
        ->addIndex( ['legacy_id'])
        ->save();
    }
            
  }
}
