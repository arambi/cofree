<?php

use Cake\Routing\DispatcherFactory;
use Cofree\Routing\Filter\CookiesFilter;


DispatcherFactory::add( new CookiesFilter(['priority' => 1]));
