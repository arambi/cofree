<?php

return [
    'inputContainer' => '<div class="form-group {{div_class}}">{{content}}</div>',
    'input' => '<input type="{{type}}" class="form-control" name="{{name}}"{{attrs}}/>',
    'inputSubmit' => '<input type="{{type}}" class="btn btn-default" {{attrs}}/>',
    'textarea' => '<textarea name="{{name}}" class="form-control" {{attrs}}>{{value}}</textarea>',
];
