<?php

return [
    'inputContainer' => '<label class="{{type}} {{div_class}} {{class}}">{{content}}</label>',
    'inputContainerError' => '<label class="{{type}} {{class}}">{{content}}</label>{{error}}',
    'formGroup' => '{{input}}{{label}}',
    'multiCheckboxFormGroup' => '<div>{{label}}</div>{{input}}',
    'label' => '<span{{attrs}}>{{text}}</span>',
    'input' => '<input type="{{type}}" name="{{name}}"{{attrs}}/>',
    'inputSubmit' => '<input type="{{type}}" class="btn btn-default" {{attrs}}/>',
    'textarea' => '<textarea name="{{name}}" class="form-control" {{attrs}}>{{value}}</textarea>',
    'checkbox' => '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>',
    'checkboxWrapper' => '<div class="checkbox {{class}}" {{attrs}}>{{label}}</div>{{after}}',
];
