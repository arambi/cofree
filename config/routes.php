<?php
use Cake\Routing\Router;

Router::plugin('Cofree', function($routes) {
	$routes->fallbacks();
});


/**
 * Templates para Angular
 */

Router::connect( '/:lang/templates/:plug/:ctrl/:file.html', [
  'plugin' => 'Cofree',
  'controller' => 'Templates',
  'action' => 'index'
], ['routeClass' => 'I18n.I18nRoute']);