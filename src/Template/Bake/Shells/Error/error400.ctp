<?php $this->layout = 'error' ?>
<div class="container">
  <div class="c-error400">
    <h2>Oops!</h2>
    <h1><?= __d( 'app', 'Error {0}', ['{404}']) ?></h1>
    <h3><?= __d( 'app', 'No podemos encontrar la página que estás buscando, por favor, verifica la dirección, o navega por las otras secciones de la web.') ?></h3>
  </div>
</div>