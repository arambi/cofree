<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->loadHelper( 'Cofree.Buffer') ?>
  <?php $this->loadHelper( 'Section.Nav') ?>
  <?= $this->element( '<%= $plugin %>.layout/head') ?>

</head>
<body class=" <?= $this->Nav->currentLang('iso2') ?>">
  <?= $this->element( '<%= $plugin %>.layout/top') ?>
  <?= $this->element( '<%= $plugin %>.layout/header') ?>
  
  <?= $this->fetch( 'content') ?>

  <?= $this->element( '<%= $plugin %>.layout/footer') ?>
</body>
<?= $this->element( '<%= $plugin %>.layout/bottom') ?>
</html>
