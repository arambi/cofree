<?php $this->loadHelper( 'Section.Nav') ?>
<div class="cookies c-cookies">
  <p><?= __d( 'app', "Utilizamos cookies propias y de terceros para mejorar la experiencia de navegación. <br/>Al continuar con la navegación entendemos que aceptas nuestra {0} política de cookies{1}.", [
      '<a href="'. $this->Nav->cnameUrl( 'cookies') .'">',
      '</a>'
  ]) ?></p>
  <span class="cookies-close c-cookies__close"><i class="fa fa-remove"></i></span>
</div>