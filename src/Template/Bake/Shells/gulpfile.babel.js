
import concat from 'gulp-concat';
import del from 'del';
import gulp from 'gulp';
import uglify from 'gulp-uglify';
import browserSync from 'browser-sync';
import sass from 'gulp-sass';
import postcss from 'gulp-postcss';
import postcssCopy from 'postcss-copy';
import cleanCSS from 'gulp-clean-css';
import fontAwesome from 'postcss-font-awesome';
import sourcemaps from 'gulp-sourcemaps';
import bulkSass from 'gulp-sass-bulk-import';

const server = browserSync.create();

const sourcesCSS = [
'./node_modules/jssocials/dist/jssocials.css',
'./node_modules/venobox/venobox/venobox.css',
'./node_modules/tarteaucitronjs/css/tarteaucitron.css',
'./webroot/css/*.scss',
'!./webroot/css/admin.scss',
'!./webroot/css/invoice.scss',
'!./webroot/css/email.scss',
'!./webroot/css/pdf.scss'
];

const sourcesJS = [
  './vendor/cofreeweb/cofree/webroot/js/jquery-2.1.4.min.js',
  './node_modules/venobox/venobox/venobox.min.js',
  './node_modules/jssocials/dist/jssocials.js',
  './vendor/cofreeweb/cofree/webroot/utils/overclick.js',
  './vendor/cofreeweb/cofree/webroot/js/utils.js',
  './vendor/cofreeweb/cofree/webroot/utils/slidebar-config.js',
  './vendor/cofreeweb/cofree/webroot/slidebars/slidebars.min.js',
  './vendor/cofreeweb/cofree/webroot/scrollto/2.1.3/jquery.scrollTo.min.js',  
  './vendor/cofreeweb/cofree/webroot/js/sticky-header.js',
  './webroot/js/tarteaucitronjs/tarteaucitron.js',
  './webroot/js/tarteaucitronjs/tarteaucitron.services.js',
  './node_modules/scrolltofixed/jquery-scrolltofixed.js'
];


const processors = [
  fontAwesome(),
  postcssCopy({
      src: ['./vendor/cofreeweb/cofree/webroot'],
      dest: './webroot/css',
      relativePath(dirname, fileMeta, result, options) {
        return './webroot/css';
      }
  })
];


const clean = () => del(['dist']);

function scripts() {
  return gulp.src( sourcesJS)
  .pipe( sourcemaps.init())
  .pipe( concat('application.js', {newLine: ';'}))
  .pipe( uglify())
  .pipe( sourcemaps.write( 'maps'))
  .pipe( gulp.dest('./webroot/js'));
}

function reload(done) {
server.reload();
done();
}




function sassWatch(){
return gulp.src( sourcesCSS)
  .pipe( sourcemaps.init())
  .pipe( bulkSass())
  .pipe( sass().on( 'error', sass.logError))
  .pipe( concat( 'application.css'))
  .pipe( postcss( processors))
  .pipe( cleanCSS())
  .pipe( sourcemaps.write( 'maps'))
  .pipe( gulp.dest( './webroot/css'))
  ;
}

function serve(done) {
  server.init({
      proxy: "http://web.web",
      open: false,
      files: ['./webroot/css/*.css'],
      port: 3902
  });
  done();
}

const watch = () => {
  gulp.watch("./webroot/css/*/*.scss", gulp.series( sassWatch));
  gulp.watch("./plugins/Web/src/Template/*/*.ctp", gulp.series( reload));
  gulp.watch("./plugins/Web/src/Template/Element/*/*.ctp", gulp.series( reload));
  gulp.watch("./plugins/Web/src/Template/Cell/*/*.ctp", gulp.series( reload));
};
const dev = gulp.series( clean, scripts, sassWatch, serve, watch);
export default dev;


gulp.task('admin', () => {
return gulp.src( './webroot/css/admin.scss')
.pipe( sourcemaps.init())
.pipe( bulkSass())
.pipe( sass().on( 'error', sass.logError))
.pipe( concat( 'admin.css'))
.pipe( cleanCSS())
.pipe( sourcemaps.write( 'maps'))
.pipe( gulp.dest( './webroot/css'))
;
});

gulp.task('email', () => {
  return gulp.src( './webroot/css/email.scss')
   .pipe( sourcemaps.init())
   .pipe( bulkSass())
   .pipe( sass().on( 'error', sass.logError))
   .pipe( concat( 'email.css'))
   .pipe( sourcemaps.write( 'maps'))
   .pipe( gulp.dest( './webroot/css'))
   ;
 });

 gulp.task('invoice', () => {
  return gulp.src( './webroot/css/invoice.scss')
   .pipe( sourcemaps.init())
   .pipe( bulkSass())
   .pipe( sass().on( 'error', sass.logError))
   .pipe( concat( 'invoice.css'))
   .pipe( sourcemaps.write( 'maps'))
   .pipe( gulp.dest( './webroot/css'))
   ;
 });