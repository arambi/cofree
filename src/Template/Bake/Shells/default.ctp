<!DOCTYPE html>
<html lang="<?= $this->Nav->currentLang( 'iso2') ?>">
<head>
  <?= $this->element( 'layout/head') ?>
</head>
<body class="section-<?= $this->request->params ['section']->id ?> <?= $this->Nav->isHomepage() ? 'homepage' : '' ?> <?= $this->Nav->bodyClass() ?>">
  <div id="wrapper" class="wrapper">
    <?= $this->element( 'layout/top') ?>
    
    <?= $this->element( 'layout/header') ?>

    <div class="container main">
      <?= $this->cell( 'Section.Wrapper::display', [
        'name' => 'main', 
        'content' => $this->fetch( 'content'),
      ]) ?>
    </div>
    
    <?= $this->element( 'layout/footer') ?>
  </div>
</body>
<?= $this->element( 'layout/bottom') ?>
</html>
