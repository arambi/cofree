<?php $this->loadHelper( 'Cofree.Socialnet') ?>
<?php $this->loadHelper( 'Block.Blockers') ?>
<footer class="o-footer">
  <div class="c-footer-addresses">
    <div class="container">
      <div class="c-footer-addresses__logos">
        <div class="c-footer-addresses__logos--social">
          <ul>
            <?= $this->Socialnet->display() ?>
          </ul>
        </div>
        <div class="c-footer-addresses__logos--access">
        </div>

      </div>
      <div class="c-footer-addresses__copyright">
        © <?= $this->Nav->website( 'title') ?> <?= date( 'Y') ?>
        <?= $this->Nav->nav( 'bottom', [], [
          'showChildrens' => true,
          'maxLevels' => 1
        ]) ?>
      </div>
    </div>
  </div>
</footer>
<a href id="to-top" data-rel="body" class="scroll-to scroll-up"><i class="fa fa-angle-double-up"></i></a>