<?= $this->Html->script([
  '/js/application.js',
]) ?>
<?= $this->fetch( 'script') ?>
<?= $this->Buffer->write() ?>

<script type="text/javascript">
function initMap(){
  <?= $this->Buffer->write( 'initMap') ?>
}
</script>


<script>
  <?php if( !empty( $this->Nav->website( 'settings.google_analytics'))): ?>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', '<?= $this->Nav->website( 'settings.google_analytics') ?>', 'auto');
      ga('send', 'pageview');
  <?php endif ?>
</script>

<script type="text/javascript">
  $(function(){
    $('.c-header-wrapper').stickMe({
      triggerAtCenter: false
    })
    .on( 'sticky-begin', function(){
      $('body').addClass( 'sticking-body')
    })
    .on( 'top-reached', function(){
      $('body').removeClass( 'sticking-body')
    })
    ; 
    $('.scroll-to').click(function(){
      $.scrollTo( $(this).data( 'rel'), 800, {
        offset: {top: -200}
      });
      return false;
    })

    
    $('body').delegate( '[active-menu]', 'click', function(e ){
      e.preventDefault();
      $(this).toggleClass( 'active');
      var $el = $('[menu=' + $(this).attr( 'active-menu') + ']');
      
      if( !$el.hasClass( 'active')) {
        $('[menu]').removeClass( 'active');
      }
  
      $el.toggleClass( 'active');
    })

    tarteaucitron.init({
        "privacyUrl": "", /* Privacy policy url */

        "hashtag": "#tarteaucitron", /* Open the panel with this hashtag */
        "cookieName": "tartaucitron", /* Cookie name */
        
        "orientation": "top", /* Banner position (top - bottom) */
        "showAlertSmall": false, /* Show the small banner on bottom right */
        "cookieslist": true, /* Show the cookie list */

        "adblocker": false, /* Show a Warning if an adblocker is detected */
        "AcceptAllCta" : true, /* Show the accept all button when highPrivacy on */
        "highPrivacy": false, /* Disable auto consent */
        "handleBrowserDNTRequest": false, /* If Do Not Track == 1, disallow all */

        "removeCredit": true, /* Remove credit link */
        "moreInfoLink": true, /* Show more info link */
        "useExternalCss": true, /* If false, the tarteaucitron.css file will be loaded */

        //"cookieDomain": ".my-multisite-domaine.fr" /* Shared cookie for subdomain website */
                              
        "readmoreLink": "/cookiespolicy" /* Change the default readmore link pointing to opt-out.ferank.eu */
    });

    tarteaucitron.user.gtagUa = '<?= $this->Nav->website( 'settings.google_analytics') ?>';
          tarteaucitron.user.gtagMore = function () { /* add here your optionnal gtag() */ };
          (tarteaucitron.job = tarteaucitron.job || []).push('gtag');

  })
</script>