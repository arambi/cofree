<?php $this->loadHelper( 'Cofree.Socialnet') ?>
<header class="c-header-wrapper">
  <div class="container">
    <div class="c-header">
      <div class="c-header__logo">
        <a class="c-header-logo__link" href="<?= $this->Nav->homelink()  ?>">
          <?#= $this->Html->image( '.svg', ['alt' => __d( 'app', 'Logotipo'), 'class' => 'logo-img']) ?>
        </a>
      </div>
      <div class="c-header__navmain">
        <nav class="c-navmain" menu="main">
          <?= $this->Nav->nav( 'main', [], [
            'showChildrens' => true,
            'maxLevels' => 1
          ]) ?>
        </nav>
      </div>
      <div class="c-nav-open-wrapper" active-menu="main">
        <div class="c-nav-open">
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
  </div>
</header>

