<div class="top-wrapper">
  <div class="container">
    <div class="top-container">
      <nav class="nav-langs">
        <?= $this->Nav->langs(['current' => false, 'fullName' => true]) ?>
      </nav>
      <nav class="navbar-top">
        <?= $this->Nav->nav( 'top', [], [
          'showChildrens' => true,
          'maxLevels' => 1
        ]) ?>
      </nav>
    </div>
  </div>
</div>