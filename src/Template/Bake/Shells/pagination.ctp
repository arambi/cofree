<?php if( $this->Paginator->param( 'pageCount') == 1) return; ?>
<nav class="c-pagination">
  <ul>
    <?php if( $this->Paginator->hasPrev()): ?>
      <?= $this->Paginator->prev( '<i class="fa fa-chevron-left"></i>', ['escape' => false, 'url' => $url]) ?>
    <?php endif ?>
    <?= $this->Paginator->numbers( ['url' => $url]) ?>
    <?php if( $this->Paginator->hasNext()): ?>
      <?= $this->Paginator->next( '<i class="fa fa-chevron-right"></i>', ['escape' => false, 'url' => $url]) ?>
    <?php endif ?>
  </ul>
</nav>