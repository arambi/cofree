<div <?= !empty( $block->settings->align) ? ' style="text-align:' . $block->settings->align .'"' : ''  ?>>
  <?php if( !empty( $block->settings->url)): ?>
      <a href="<?= $block->settings->url ?>">
  <?php endif ?>
  <?= $this->Uploads->image( $block->photo, 'big') ?>
  <?php if( !empty( $block->settings->url)): ?>
      </a>
  <?php endif ?>
</div>
