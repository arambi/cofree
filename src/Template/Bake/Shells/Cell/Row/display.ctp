
<?php foreach( $rows as $key => $row): ?>
  <?php if( !empty( $row->settings->wrapper_div)): ?>
      <div id="" class="<?= $row->settings->wrapper_div_class ?>">
  <?php endif ?>

  <?php if( $this->Nav->isFullwidth() && !(isset( $row->settings->fullwidth) && $row->settings->fullwidth)): ?>
      <div class="container container-row">
  <?php endif ?>
  <div <?= $this->Blockers->attrs( $row, ['class' => 'row'], 'row', $key) ?>>
    <?php foreach( $row->columns as $key2 => $column): ?>
      <div <?= $this->Blockers->attrs( $column, ['class' => 'col col-xs-12 col-sm-' . $this->Blockers->columns( $column)], 'col', $key2) ?>>
        <?php foreach( $column->blocks as $key3 => $block): ?>
            <div <?= $this->Blockers->attrs( $block, ['blockClass' => 'o-block', 'classPrefix' => 'b'], 'block', $key3) ?>>
              <?= $this->cell( $block->block_type ['cell'], ['block' => $block]) ?>
            </div>
        <?php endforeach ?>
      </div>
    <?php endforeach ?>
  </div>
  <?php if( $this->Nav->isFullwidth() && !@$row->settings->fullwidth): ?>
      </div>
  <?php endif ?>

  <?php if( !empty( $row->settings->wrapper_div)): ?>
      </div>
  <?php endif ?>
<?php endforeach ?>
