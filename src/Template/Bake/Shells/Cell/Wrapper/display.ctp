<?php foreach( $rows as $row): ?>
  <?php if( $this->Nav->isFullwidth() && !$row->hasContent() && !(isset( $row->settings->fullwidth) && $row->settings->fullwidth)): ?>
      <div class="container container-row">
  <?php endif ?>
  <div <?= $this->Blockers->attrs( $row, ['class' => 'row']) ?>>
    <?php foreach( $row->columns as $column): ?>
      <div <?= $this->Blockers->attrs( $column, ['class' => 'col col-xs-12 '. $this->Blockers->responsiveColumns( $column)]) ?>>
        <?php foreach( $column->blocks as $block): ?>
            <div <?= $this->Blockers->attrs( $block, ['blockClass' => 'o-block', 'classPrefix' => 'b']) ?>>
              <?= $this->cell( $block->block_type ['cell'], ['block' => $block, 'content' => $content]) ?>
            </div>
        <?php endforeach ?>
      </div>
    <?php endforeach ?>
  </div>
  <?php if( $this->Nav->isFullwidth() && !$row->hasContent() && !(isset( $row->settings->fullwidth) && $row->settings->fullwidth)): ?>
      </div>
  <?php endif ?>
<?php endforeach ?>