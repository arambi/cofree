    <div class="cookies-ct">
      <p><?= sprintf( "Utilizamos cookies propias y de terceros para mejorar la experiencia de navegación. <br/>Al continuar con la navegación entendemos que aceptas nuestra %s política de cookies%s.", 
          '<a href="/es/politica-de-cookies">',
          '</a>'
      )?></p>
    </div>
