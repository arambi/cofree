<?php

namespace Cofree\Model\Entity;

trait ErrorsManagerTrait
{
  public function getErrorsByField()
  {
    $errors = $this->getErrors();

    $return = [];

    foreach( $errors as $key => $error)
    {
      $return [$key] = array_values( $error);
    }

    return $return;
  }
}