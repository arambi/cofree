<?php 

namespace Cofree\Model\Entity;

trait UrlsEntityTrait
{

/**
 * Devuelve el nombre de una URL, eliminando el protocolo http
 *
 * @param string $property
 * @return string
 */
  public function urlName( $property)
  {
    $url = $this->$property;

    return str_replace( ['http://', 'https://'], '', $url);
  }


/**
 * Devuelve el la URL dado un string. Le incluye el protocolo en caso de no tenerlo
 *
 * @param string $property
 * @return string
 */
  public function urlLink( $property)
  {
    $url = $this->$property;

    if( strpos( $url, '://') === false)
    {
      $url = 'http://'. $url;
    }

    return $url;
  }
}