<?php
namespace Cofree\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContentsContent Entity
 *
 * @property int $id
 * @property int $content_id
 * @property int $foreign_id
 * @property \Cake\I18n\Time $created
 * @property int|null $position
 *
 * @property \Cofree\Model\Entity\Content $content
 * @property \Cofree\Model\Entity\Foreign $foreign
 */
class ContentsContent extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'content_id' => true,
        'foreign_id' => true,
        'created' => true,
        'position' => true,
        'content' => true,
        'foreign' => true,
    ];
}
