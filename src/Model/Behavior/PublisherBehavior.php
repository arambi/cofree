<?php
namespace Cofree\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\Query;

/**
 * Publisher behavior
 *
 * Colección de métodos para determinar si un contenido está o no publicado
 */
class PublisherBehavior extends Behavior
{
  protected $_defaultConfig = [
    'implementedMethods' => [
      'publishedQuery' => 'publishedQuery',
      'fromQuery' => 'fromQuery',
      'fromToQuery' => 'fromToQuery',
      'published' => 'findPublished'
    ],
    'implementedFinders' => [
      'published' => 'findPublished',
      'from' => 'findFrom'
    ]
  ];

  public function initialize( array $config) 
  {
    if( $this->_table->hasField( 'published_at'))
    {
      $this->_table->crud->defaults([
        'published_at' => date( 'Y-m-d'),
      ]);
    }
  }

  public function findPublished( Query $query)
  {
    return $query->where([
      $this->_table->alias() .'.published' => true
    ]);
  }
/**
 * Añade el parámetro de publicado
 * 
 * @param  Cake\Database\Query $query
 * @return Cake\ORM\Table
 */
  public function publishedQuery( $query)
  {
    $query->where([
      $this->_table->alias() .'.published' => true
    ]);

    return $this->_table;
  }

/**
 * Añade el parámetro de publicado desde. Para contenidos tipo noticia
 * 
 * @param  Cake\Database\Query $query
 * @return Cake\ORM\Table
 */
  public function fromQuery( $query)
  {
    $query->where([
      $this->_table->alias() .'.published_at < NOW'
    ]);

    return $this->_table;
  }

  public function findFrom( Query $query)
  {
    return $query->where([
      $this->_table->alias() .'.published_at <= ' => date( 'Y-m-d')
    ]);
  }

/**
 * Añade los parámetros desde y hasta, teniendo en cuenta que cada uno de ellos puede ser null (en ese caso no se tiene en cuenta)
 * 
 * @param  Cake\Database\Query $query
 * @return Cake\ORM\Table
 */
  public function fromToQuery( $query)
  {
    $query
      ->andWhere( function( $exp, $query) {
        return $exp
          ->or_([
            $this->_table->alias() .'.date_restrict' => 0,
            $this->_table->alias() .'.start_on IS NULL',
            $this->_table->alias() .'.start_on < NOW()',
          ]);
      })
      ->andWhere( function( $exp, $query) {
        return $exp
          ->or_([
            $this->_table->alias() .'.date_restrict' => 0,
            $this->_table->alias() .'.finish_on IS NULL',
            $this->_table->alias() .'.finish_on > NOW()',
          ]);
      });

    return $this->_table;
  }
}
