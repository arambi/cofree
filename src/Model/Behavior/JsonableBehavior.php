<?php
namespace Cofree\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\Entity;
use Cake\Event\Event;
use ArrayObject;
use Cofree\Model\Behavior\JsonableTrait;
use Cake\Datasource\EntityInterface;
use Cake\Database\Type;

Type::map( 'jsonable', '\Cofree\Database\Type\JsonType');
/**
 * Jsonable behavior
 */
class JsonableBehavior extends Behavior 
{

  use JsonableTrait;

  public function initialize( array $config)
  {
    $this->setConfig( 'implementedMethods', [
      'isJsonableField' => 'isJsonableField'
    ]);

    $schema = $this->_table->getSchema();

    foreach( $this->getConfig( 'fields') as $field)
    {
      $schema->setColumnType( $field, 'jsonable');
    }
  }

  public function implementedEvents()
  {
    $events = parent::implementedEvents();
    $events ['Crud.afterSetEmptyProperties'] = 'afterSetEmptyProperties';
    return $events;
  }


  public function isJsonableField( $field)
  {
    $fields = $this->translateFields();
    return in_array( $key, $fields);
  }
}
