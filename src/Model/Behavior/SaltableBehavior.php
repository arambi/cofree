<?php

namespace Cofree\Model\Behavior;

use ArrayObject;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\Core\Configure;
use Cake\Utility\Security;
use Hidehalo\Nanoid\Client;

/**
 * Saltable behavior
 */
class SaltableBehavior extends Behavior
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'field' => 'salt',
        'size' => 16
    ];

    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $field = $this->getConfig('field');

        if (
            !$entity->has('id')
            && $this->_table->hasField($field)
            && (!$entity->has($field) || empty($entity->$field))
        ) {
            $entity->set($field, $this->generateSalt());
        }
    }

    public function generateSalt()
    {
        $client = new Client();
        $salt = $client->generateId($this->getConfig('size'), Client::MODE_DYNAMIC);

        if (!$this->_table->exists([$this->getConfig('field') => $salt])) {
            return $salt;
        } else {
            return $this->generateSalt();
        }
    }
}
