<?php

namespace Cofree\Model\Behavior;

use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;

/**
 * Sortable behavior
 */
class SortableBehavior extends Behavior
{
    public function initialize(array $config)
    {
        $this->_table->crud->sortable(true);
    }

    public function beforeFind(Event $event, Query $query)
    {
        if ($this->_table->hasField('position')) {
            $this->_hasField = false;

            /** @var Cake\Database\Expression\OrderByExpression $order */
            $order = $query->clause( 'order');

            if ($order) {
                $order->iterateParts(function($direction, $condition) {
                    if (in_array($condition, ['position', $this->_table->getAlias() . '.position'])) {
                        $this->_hasField = true;
                        return $direction;
                    }
                });
            }

            if (!$this->_hasField) {
                $query->order([$this->_table->getAlias() . '.position' => 'asc']);
            }
        }
    }
}
