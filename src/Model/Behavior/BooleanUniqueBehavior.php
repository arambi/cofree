<?php
namespace Cofree\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\Database\Expression\QueryExpression;
/**
 * BooleanUnique behavior
 *
 * Cuando se guarda en una columna booleana un valor 1, se asignarán a todos los registros del model el valor 0. 
 * Este Behavior es utilizado para aquellos casos en lo que queremos tener un valor que solo puede darse en un registro. 
 * Por ejemplo, la página homepage de las secciones, solo puede haber una y cuando se marca como tal, el registro que tuviera el valor 1, 
 * debe ser automáticamente marcado a 0.
 */

class BooleanUniqueBehavior extends Behavior
{

  protected $_defaultConfig = [];

/**
 * Recorre los campos definidos en la configuración y si el valor es 1, pone el resto de registros que sean de valor 1, a valor 0
 *   
 * @param  Event  $event  
 * @param  Entity $entity 
 * @return void
 */
  public function afterSave( Event $event, Entity $entity)
  {
    $fields = $this->config( 'fields');

    foreach( $fields as $field)
    {
      if( $entity->has( $field))
      {
        if( $entity->$field == 1)
        {
          $query = new QueryExpression();
          $query->add( [ $field => 1]);
          $query->not( [ $this->_table->primaryKey() => $entity->id]);
          $this->_table->updateAll( [ $field => 0], $query);
        }
      }
    }
  }
}
