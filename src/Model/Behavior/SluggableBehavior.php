<?php

namespace Cofree\Model\Behavior;

use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\Utility\Inflector;
use Cake\I18n\I18n;
use Cake\Core\Configure;

/**
 * Slug behavior
 */
class SluggableBehavior extends Behavior
{
    /**
     *
     * @var type 
     */
    protected $_defaultConfig = [
        'field' => 'title',
        'slug' => 'slug',
        'replacement' => '-',
        'implementedFinders' => [
            'slugged' => 'findSlug',
            'slug' => 'findSlug',
            'check' => 'checkExist'
        ]
    ];

    /**
     * 
     * @param Entity $entity
     */
    public function slug(Entity $entity)
    {
        $config = $this->getConfig();

        if (is_array($config['field'])) {
            $title = implode(' ', $entity->extract($config['field']));
        } elseif (is_callable($config['field'])) {
            $func = $config['field'];
            $title = $func($entity);
        } else {
            $title = $entity->get($config['field']);
        }

        $slug = $this->_doSlug($config, $title);
        $slug = $this->unique($slug, $entity->id);

        if (!empty($slug)) {
            $entity->set($config['slug'], $slug);
        }
    }

    private function unique($slug, $id = null, $number = 0)
    {
        $config = $this->getConfig();

        if ($number > 0) {
            $slug .= '-' . $number;
        }

        $query = $this->_table->find()
            ->where([
                $this->_table->getAlias() . '.' . $config['slug'] => $slug,
            ]);

        if ($id) {
            $query->where([
                $this->_table->getAlias() . '.' . $this->_table->getPrimaryKey() . ' !=' => $id
            ]);
        }

        if ($query->count() == 0) {
            return $slug;
        }

        $number++;
        return $this->unique($slug, $id, $number);
    }

    private function _hasTranslation()
    {
        if ($this->_table->hasBehavior(Configure::read('I18n.behaviorName'))) {
            $fields = $this->_table->translateFields();

            if (in_array($this->config('field'), $fields)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 
     * @param type $config
     * @param type $title
     * @return type
     */
    private function _doSlug($config, $title)
    {
        return Inflector::slug($this->_removeSign($title), $config['replacement']);
    }

    /**
     * 
     * @param Query $query
     * @param array $options
     * @return type
     */
    public function findSlug(Query $query, array $options)
    {
        $config = $this->config();
        return $query->where([$config['slug'] => $options['slug']]);
    }

    /**
     * 
     * @param Query $query
     * @param array $options
     * @return type
     */
    public function checkExist(Query $query, array $options)
    {
        return $this->findSlug($query, $this->_doSlug($this->config(), $options)) != null;
    }

    /**
     * 
     * @param Event $event
     * @param Entity $entity
     */
    public function beforeSave(Event $event, Entity $entity)
    {
        $config = $this->config();
        $slug = $entity->get($config['slug']);

        if (empty($slug)) {
            $this->slug($entity);
        } elseif ($slug) {
            $slug = Inflector::slug(strtolower($slug));
            $slug = $this->_doSlug($config, $slug);
            $slug = $this->unique($slug, $entity->id);
            $entity->set($config['slug'], $slug);
        }
    }

    /**
     * 
     * @param type $str
     * @return string
     */
    protected function _removeSign($str)
    {
        if (!$str)
            return;
        $signed = array(
            "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ",
            "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ",
            "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
            "ỳ", "ý", "ỵ", "ỷ", "ỹ",
            "đ",
            "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
            "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
            "Ì", "Í", "Ị", "Ỉ", "Ĩ",
            "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
            "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
            "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
            "Đ", "ê", "ù", "à"
        );
        $unsigned = array(
            "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
            "i", "i", "i", "i", "i",
            "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o",
            "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
            "y", "y", "y", "y", "y",
            "d",
            "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A",
            "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
            "I", "I", "I", "I", "I",
            "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O",
            "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",
            "Y", "Y", "Y", "Y", "Y",
            "D", "e", "u", "a"
        );
        return mb_strtolower(str_replace($signed, $unsigned, $str));
    }
}
