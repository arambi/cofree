<?php
namespace Cofree\Model\Behavior;

use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;


/**
 * Contentable behavior
 */
class ContentableBehavior extends Behavior 
{

/**
 * Default configuration.
 *
 * @var array
 */
	protected $_defaultConfig = [];

  protected $_hasField = false;

  public function initialize(array $config)
  {
    if( isset( $config ['type']))
    {
      $this->config( 'type', $config ['type']);
    }
    else
    {
      $this->config( 'type', $this->_table->alias());
    }
  }


/**
 * Coloca el valor del ContentType, que será el id del registro que tiene el nombre del model
 * @param  Event       $event   
 * @param  Entity      $entity  
 * @param  ArrayObject $options 
 * @return void
 */
  public function beforeSave( Event $event, Entity $entity)
  {
    $entity->set( 'content_type', $this->config( 'type'));
  }
  
/**
 * Añade a las condiciones de búsqueda el nombre del model
 * @param  Event  $event 
 * @param  Query  $query 
 * @return void        
 */
  public function beforeFind( Event $event, Query $query)
  {
    $this->_hasField = false;

    if( $this->_table->hasField( 'content_type'))
    {
      $where = $query->clause( 'where');
      
      if( $where instanceof \Cake\Database\Expression\QueryExpression)
      {
        $query->clause( 'where')->iterateParts( 
          function( $part)
          {
            if( $part instanceof \Cake\Database\Expression\Comparison)
            {
              $field = $part->getField();

              if( $field == $this->_table->primaryKey())
              {
                $part->setField( $this->_table->alias() .'.'. $field);
              }

              if( strpos( $field, 'content_type') !== false)
              {
                $this->_hasField = true;
              }
            }
            
            if( $part instanceof \Cake\Database\Expression\QueryExpression)
            {
              $this->__iterateParts( $part);
            }

            return $part;
          }
        );
      }

      if( !$this->_hasField)
      {
        $query->where( [$this->_table->alias() . '.content_type' => $this->config( 'type')]);
      }
    }
  }

  private function __iterateParts( $part)
  {
    $part->iterateParts(
      function( $part)
      {
        if( $part instanceof \Cake\Database\Expression\Comparison)
        {
          $field = $part->getField();

          if( $field == $this->_table->primaryKey())
          {
            $part->setField( $this->_table->alias() .'.'. $field);
          }

          if( strpos( $field, 'content_type') !== false)
          {
            $this->_hasField = true;
          }
        }

        if( $part instanceof \Cake\Database\Expression\QueryExpression)
        {
          $this->__iterateParts( $part);
        }
        return $part;
      }
    );
  }

}
