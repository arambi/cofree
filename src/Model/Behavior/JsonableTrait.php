<?php

namespace Cofree\Model\Behavior;
  
use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\Entity;
use Cake\Event\Event;
use \ArrayObject;

/**
 * Usado por aquellos behaviors que usan JSON para guardar datos, como JsonableBehavior y UploadJsonableBehavior
 */
trait JsonableTrait
{

  public function afterSetEmptyProperties( Event $event, ArrayObject $data)
  {
    foreach( $this->config( 'fields') as $field)
    {
      if( !isset($data[$field]))
      {
        $data [$field] = null;
      }
      elseif( is_string( $data [$field]))
      {
        $data [$field] = json_decode( $data [$field], true);
      }
    }
  }

  public function beforeFind( Event $event, Query $query)
  {
    $query->formatResults(function ($results) {
      return $this->_rowMapper( $results);
    }, $query::PREPEND);
  }


  protected function _rowMapper( $results)
  {
    return $results->map(function( $result){
      foreach( $this->config( 'fields') as $field)
      {
        if( isset( $result->$field) && is_string( $result->$field))
        {
          $value = json_decode( $result->$field);
          $result->set( $field, $value);
        }
      }

      return $result;
    });
  }
}
