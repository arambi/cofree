<?php

namespace Cofree\Model\Behavior;

use ArrayObject;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\Datasource\EntityInterface;

class DeletableBehavior extends Behavior
{


    protected $_defaultConfig = [
        'implementedFinders' => [
            'regardless' => 'findRegardless',
        ],
    ];

    public function beforeDelete(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        $this->_table->query()->update()
            ->set([
                'deleted' => true
            ])
            ->where([
                $this->_table->getPrimaryKey() => $entity->get($this->_table->getPrimaryKey())
            ])
            ->execute();

        $event->stopPropagation();
    }

    public function beforeFind(Event $event, Query $query)
    {
        $this->hasDeleteInField = false;

        $where = $query->clause('where');
        
        if ($where instanceof \Cake\Database\Expression\QueryExpression) {
            $query->clause('where')->iterateParts(function ($part) {

                    if ($part instanceof \Cake\Database\Expression\Comparison) {
                        $field = $part->getField();

                        if ($field == 'deleted' || $field == $this->_table->getAlias() .'.deleted') {
                            $this->hasDeleteInField = true;
                        }
                    } elseif ($part instanceof \Cake\Database\Expression\QueryExpression) {
                    }

                    return $part;
                }
            );
        }

        if (!$this->hasDeleteInField) {
            $query->where([
                $this->_table->getAlias() . '.deleted' => false
            ]);
        }
    }

    public function findRegardless( Query $query)
    {
        return $query->where([
            $this->_table->getAlias() .'.deleted IN' => [0,1]
        ]);
    }
}
