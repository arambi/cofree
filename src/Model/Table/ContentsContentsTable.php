<?php
namespace Cofree\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ContentsContents Model
 *
 * @property \Cofree\Model\Table\ContentsTable&\Cake\ORM\Association\BelongsTo $Contents
 * @property \Cofree\Model\Table\ForeignsTable&\Cake\ORM\Association\BelongsTo $Foreigns
 *
 * @method \Cofree\Model\Entity\ContentsContent get($primaryKey, $options = [])
 * @method \Cofree\Model\Entity\ContentsContent newEntity($data = null, array $options = [])
 * @method \Cofree\Model\Entity\ContentsContent[] newEntities(array $data, array $options = [])
 * @method \Cofree\Model\Entity\ContentsContent|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Cofree\Model\Entity\ContentsContent saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Cofree\Model\Entity\ContentsContent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Cofree\Model\Entity\ContentsContent[] patchEntities($entities, array $data, array $options = [])
 * @method \Cofree\Model\Entity\ContentsContent findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ContentsContentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('contents_contents');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }
}
