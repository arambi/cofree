<?php 

namespace Cofree\Model\Traits;

trait PostViewTrait
{
  public function addView( $entity)
  {
    $count = $entity->get( 'view_count');
    $count++;

    $this->query()->update()
      ->set( 'view_count', $count)
      ->where([
        'id' => $entity->id
      ])
      ->execute();
  }
}