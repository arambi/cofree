<?php
namespace Cofree\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Assets helper
 */
class AssetsHelper extends Helper
{

  public $helpers = ['Html'];

  private $__jsPaths = [
    'bootstrap' => 'js',

  ];

  public function css( $library, $version)
  {
    return $this->Html->css( '/cofree/css/'. $library .'/'. $version .'/'. $this->__paths [$library]);
  }

  public function js( $library, $version)
  {
    return $this->Html->css( '/cofree/js/'. $library .'/'. $version .'/'. $this->__paths [$library]);
  }


}
