<?php
namespace Cofree\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * ExtraForm helper
 */
class ExtraFormHelper extends Helper
{

  public function months( $format = '%B')
  {
    return [
      '01' => __d( 'core', 'enero'), 
      '02' => __d( 'core', 'febrero'),  
      '03' => __d( 'core', 'marzo'),  
      '04' => __d( 'core', 'abril'),  
      '05' => __d( 'core', 'mayo'),  
      '06' => __d( 'core', 'junio'),  
      '07' => __d( 'core', 'julio'),  
      '08' => __d( 'core', 'agosto'),  
      '09' => __d( 'core', 'septiembre'),  
      '10' => __d( 'core', 'octubre'),  
      '11' => __d( 'core', 'noviembre'),  
      '12' => __d( 'core', 'diciembre'), 
    ];
  }
}
