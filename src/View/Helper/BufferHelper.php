<?php

namespace Cofree\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\Core\Configure;


/**
 * Buffer helper
 */
class BufferHelper extends Helper
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    private $__script = 'Buffer';
    private $__top = 'Buffer.top';

    private $key = false;

    public function scriptStart()
    {
        $this->start();
    }

    public function scriptEnd()
    {
        $this->end();
    }

    public function writeScript()
    {
        $this->write();
    }

    public function startTop()
    {
        ob_start();
    }

    public function endTop()
    {
        $buffer = ob_get_clean();

        if (!Configure::read($this->__top)) {
            Configure::write($this->__top, []);
        }

        $total = Configure::read($this->__top);
        $total[] = $buffer;
        Configure::write($this->__top, $total);
    }


    public function start($key = false)
    {
        $this->key = $key;
        ob_start();
    }

    public function end()
    {
        $buffer = ob_get_clean();

        $key = $this->key ? $this->key : 'script';

        if (!Configure::read($this->__script . '.' . $key)) {
            Configure::write($this->__script . '.' . $key, []);
        }

        $total = Configure::read($this->__script . '.' . $key);
        $total[] = $buffer;
        Configure::write($this->__script . '.' . $key, $total);
    }

    public function write($key = false)
    {
        if (!$key) {
            $key = 'script';
        }

        $scripts = Configure::read($this->__script . '.' . $key);

        if (!$scripts) {
            return '';
        }

        return implode("\n", $scripts);
    }

    public function clear($key)
    {
        Configure::delete($this->__script . '.' . $key);
    }

    public function writeTop()
    {
        $html = Configure::read($this->__top);
        if (!$html) {
            return '';
        }

        return implode("\n", $html);
    }
}
