<?php
namespace Cofree\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Angular helper
 * Utilidades para AngularJS
 */
class AngularHelper extends Helper
{

/**
 * Devuelve un template de angular, dado una ruta a un element y un nombre para el id
 * 
 * @param  string $element 
 * @param  string $id     
 * @return string          HTML
 */
  public function template( $element, $id)
  {
    $html = $this->_View->element( $element);

    return '<script type="text/ng-template" id="'. $id .'">'. $html . '</script>';
  }

}
