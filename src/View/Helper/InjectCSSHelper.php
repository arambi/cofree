<?php
namespace Cofree\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * InjectCSS helper
 */
class InjectCSSHelper extends Helper
{
  private $cssFile;

  public function start( $css)
  {
    $this->cssFile = $css;
    ob_start();
  }

  public function end()
  {
    $content = ob_get_clean();
    $css = file_get_contents( ROOT .DS. 'webroot' .DS. 'css' .DS. $this->cssFile);
    $emogrifier = \Pelago\Emogrifier\CssInliner::fromHtml($content);
    $emogrifier->inlineCss($css);
    echo $emogrifier->renderBodyContent();
  }

}
