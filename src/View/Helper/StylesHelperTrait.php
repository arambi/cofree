<?php 

namespace Cofree\View\Helper;

/**
 * Métodos para renderizar estilos en línea a partir de datos en la base de datos
 */


trait StylesHelperTrait
{
  /**
 * Devuelve el valor de la propiedad para el stilo dado
 * Toma el valor de la base de datos y lo transforma en un estilo CSS, en base a lo descrito en $this->styles
 * 
 * @param  Block\Model\Entity\Block   $block  
 * @param  string                     $field  
 * @param  string                     $prop   
 * @param  ArrayObject                $styles
 * @return void         
 */
  public function getPropertyValue( $block, $field, $prop, $styles)
  {
    if( !isset( $block->$field))
    {
      return;
    }


    if( strpos( $prop, '()') !== false)
    {
      $method = str_replace( '()', '', $prop);
      $this->$method( $block, $block->$field, $styles);
    }
    else
    {
      if( !empty( $block->$field))
      {
        $styles [] = str_replace( '%s', $block->$field, $prop);
      }
    }
  }

/**
 * Método para devolver el valor de border-radius
 * 
 * @param  string         $value 
 * @param  ArrayObject    $styles 
 * @return void
 */
  public function borderRadius( $block, $value, $styles)
  {
    if( empty( $block->with_border))
    {
      return;
    }

    $styles [] = '-webkit-border-radius: ' .$value;
    $styles [] = '-moz-border-radius: ' .$value;
    $styles [] = 'border-radius: ' .$value;
  }

  public function borderColor( $block, $value, $styles)
  {
    if( empty( $block->with_border))
    {
      return;
    }

    $styles [] = 'border-color:'. $value;
  }

  public function borderStyle( $block, $value, $styles)
  {
    if( empty( $block->with_border))
    {
      return;
    }
    
    $styles [] = 'border-style:'. $value;
  }

/**
 * Método para devolver el valor de background-size
 * 
 * @param  string         $value 
 * @param  ArrayObject    $styles 
 * @return void
 */
  public function backgroundSize( $block, $value, $styles)
  {
    if( $value == 'no-repeat')
    {
      $styles [] = 'background-repeat: no-repeat';
    }
    elseif( $value == 'repeat')
    {
      $styles [] = 'background-repeat: repeat';
    }
    elseif( $value == 'cover')
    {
      $styles [] = 'background-repeat: no-repeat';
      $styles [] = 'background-size: cover';
      $styles [] = 'background-attachment: fixed';
    }
    elseif( $value == 'fullwidth')
    {
      $styles [] = 'background-repeat: no-repeat';
      $styles [] = 'background-size: 100% auto';
      $styles [] = 'background-attachment: fixed';
    }
  }

  public function backgroundImage( $block, $value, $styles)
  {
    $styles [] = 'background-image: url('. $value->paths->org .')';

    $horizontal = !empty( $block->background_position_horizontal) ? $block->background_position_horizontal : 'left';
    $vertical = !empty( $block->background_position_vertical) ? $block->background_position_vertical : 'top';
    $styles [] = 'background-position: '. $horizontal .' '. $vertical;
  }


  public function textShadow( $block, $field, $styles)
  {
    $angle   = $block->text_shadow_angle * ((pi())/180);
    $x       = round( $block->text_shadow_distance * cos( $angle));
    $y       = round( $block->text_shadow_distance * sin( $angle));
    $blur    = round( $block->text_shadow_blur);
    $opacity = (round( $block->text_shadow_opacity * 100) / 1000); // 2dp

    $styles [] = 'text-shadow: '. $x  . 'px ' . $y  . 'px ' . $blur . 'px rgba(0,0,0,'. $opacity .')';
  }
}