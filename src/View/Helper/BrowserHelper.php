<?php
namespace Cofree\View\Helper;

use Browser;
use Cake\View\View;
use Cake\View\Helper;
use Cake\Utility\Inflector;

/**
 * Browser helper
 */
class BrowserHelper extends Helper
{
  /**
   * Default configuration.
   *
   * @var array
   */
  protected $_defaultConfig = [];

  public function bodyClass()
  {
    $browser = new Browser();
    $name = $browser->getBrowser();
    $version = $browser->getVersion();
    $version = substr( $version, 0, strpos( $version, '.'));
    return 'browser-' . Inflector::slug( strtolower( $name)) . ' version-'. $version;
  }

}
