<?php
namespace Cofree\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Calendar helper
 */
class CalendarHelper extends Helper
{

  public function template( $template = null)
  {
    if( $template == null)
    {
      $template = 'Cofree.calendar';
    }

    return $this->_View->element( $template);
  }

}
