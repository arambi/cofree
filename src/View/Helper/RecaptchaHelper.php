<?php
namespace Cofree\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\Core\Configure;
use I18n\Lib\Lang;

/**
 * Recaptcha helper
 */
class RecaptchaHelper extends Helper
{

  /**
   * Default configuration.
   *
   * @var array
   */
  public $helpers = ['Html'];

  public $sessionKey = 'ValidRecaptcha';

  public function recaptcha()
  {
    $vars = $this->_View->getVars();

    if( $this->request->session()->check( $this->sessionKey))
    {
      return null;
    }

    $out = [];
    $out [] = '<div class="g-recaptcha" data-sitekey="'. Configure::read( 'Recaptcha.key') .'"></div>';
    $out [] = $this->Html->script( 'https://www.google.com/recaptcha/api.js?hl=' . Lang::current( 'iso2'));

    if( in_array( 'recaptchaError', $vars))
    {
      $out [] = '<div class="error-message">'. $this->_View->get( 'recaptchaError') .'</div>';
    }

    return implode( "\n", $out);
  }

}
