<?php

namespace Cofree\View\Helper;

use Cake\View\View;
use Cake\View\Helper;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Website\Lib\Website;

/**
 * Gtm helper
 */
class Gtm2Helper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public $helpers = [
        'Section.Nav',
        'User.Auth',
    ];

    public function analyticsScript()
    {
        if (Website::get('settings.google_analytics') && Website::get('settings.google_analytics_4')) {
            $id = Website::get('settings.google_analytics');
            $id4 = Website::get('settings.google_analytics_4');

            $out = <<<EOF
                <!-- Universal Analytics y Analytics 4 -->
                <script type='text/javascript' src='https://www.googletagmanager.com/gtag/js?id=$id' id='google_gtagjs-js' async></script>
                <script type='text/javascript' id='google_gtagjs-js-after'>
                    window.dataLayer = window.dataLayer || [];

                    function gtag() {
                    dataLayer.push(arguments);
                    }
                    gtag("js", new Date());
                    gtag("config", "$id", {
                    "anonymize_ip": true
                    });
                    gtag("config", "$id4");
                    dataLayer.push({ ecommerce: null });
                </script>
                <!-- Fin Analytics -->
            EOF;
        } elseif (Website::get('settings.google_analytics')) {
            $id = Website::get('settings.google_analytics');

            $out = <<<EOF
                <!-- Universal Analytics -->
                <script>
                    (function(i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function() {
                        (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
                
                    ga('create', '$id', 'auto');
                    ga('send', 'pageview');
                </script>
                <!-- Fin Analytics -->
            EOF;
        } elseif (Website::get('settings.google_analytics_4')) {
            $id4 = Website::get('settings.google_analytics_4');

            $out = <<<EOF
                <!-- Analytics 4 -->
                <script async src="https://www.googletagmanager.com/gtag/js?id=$id4"></script>
                <script>
                    window.dataLayer = window.dataLayer || [];

                    function gtag() {
                        dataLayer.push(arguments);
                    }
                    gtag('js', new Date());

                    gtag('config', '$id4');
                </script>
                <!-- Fin Analytics -->
            EOF;
        }

        return $out;
    }

    public function gtmScript()
    {
        $id = Website::get('settings.google_gtm');

        if (!empty($id)) {
            $out = <<<EOF
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','$id');</script>
EOF;

            return $out;
        }
    }


    public function gTag($data, $event_name)
    {
        $json = json_encode($data);
        $out = <<<EOF
    gtag('event', '$event_name', $json);
EOF;

        return '<script>' . $out . '</script>';
    }

    public function layer($data, $action, $event = false, $extra = [], $script_tag = true)
    {
        $layer = [];

        if ($event) {
            $layer['event'] = $event;
        }


        if ($action) {
            $layer['ecommerce'][$action] = $data;
        } else {
            $layer['ecommerce'] = $data;
        }

        $layer = array_merge($layer, $extra);
        $json = json_encode($layer);



        $out = <<<EOF
        dataLayer.push({ecommerce: null});
        dataLayer.push($json);
        EOF;

        // DEBUG
        if ($event == 'purchase') {
            \Cake\Log\Log::debug("\n ---> Layer \n " . $out);
        }

        if ($script_tag) {
            return '<script>' . $out . '</script>';
        } else {
            return $out;
        }
    }

    public function purchase($order, $callback = false, $extra = [])
    {
        $data = [
            'affiliation' => Website::get('title'),
            'transaction_id' => (string)$order->id,
            'value' => number_format($order->total, 2, '.', ''),
            'tax' => number_format($order->taxes, 2, '.', ''),
            'shipping' => number_format($order->realPrice($order->shipping_handling, $order->shipping_handling_tax_rate), 2, '.', ''),
            // 'value' => (string)str_replace(',', '.', $order->total),
            // 'tax' => (string)str_replace(',', '.', $order->taxes),
            // 'shipping' => (string)str_replace(',', '.', $order->realPrice($order->shipping_handling, $order->shipping_handling_tax_rate)),
            'items' => $this->orderItems($order, $callback),
        ];

        if ($order->coupon_name) {
            $data['coupon'] = $order->coupon_name;
        }

        $data = array_merge($data, $extra);

        $user = [
            'customer' => [
                'id' => $order->adr_invoice_vat_number,
                'new_customerd' => TableRegistry::getTableLocator()->get('Store.Orders')->find('completed')
                    ->where([
                        'Orders.adr_invoice_vat_number' => $order->adr_invoice_vat_number,
                        'Orders.id !=' => $order->id
                    ])
                    ->count() == 0,
                'billing' => [
                    'first_name' => $order->adr_invoice_firstname,
                    'last_name' => $order->adr_invoice_lastname,
                    'company' => $order->adr_invoice_company,
                    'address_1' => $order->adr_invoice_address,
                    'city' => $order->adr_invoice_city,
                    'state' => $order->state_invoice->title,
                    'country' => $order->country_invoice->iso_code,
                    'email' => $order->order_email,
                    'emailhash' => md5($order->order_email),
                    'phone' => '34' . str_replace(' ', '', $order->adr_delivery_phone),
                ]
            ]
        ];

        return $this->layer($data, null, 'purchase');
        // return $this->layer($data, null, 'purchase', $user);
    }

    public function cartOpen($order, $callback = false, $extra = [])
    {
        $order = TableRegistry::getTableLocator()->get('Store.Orders')
            ->findBySalt($order['salt'])
            ->find('order')
            ->first();

        $data = [
            'items' => $this->orderItems($order, $callback),
        ];

        $data = array_merge($data, $extra);
        return $this->layer($data, null, 'view_cart', [], false);
    }

    public function beginCheckout($order, $custom = [], $callback = false)
    {
        $data = $this->orderItems($order, $callback);
        return $this->layer($data, 'items', 'begin_checkout');
    }

    public function checkoutProgress($order, $custom = [], $callback = false)
    {
        $data = $this->orderData($order, $custom, $callback);
        return $this->gTag($data, 'checkout_progress');
    }

    public function productView($product, $custom = [])
    {
        $custom['item_list_name'] = 'Página de producto';
        $custom['item_list_id'] = 'Página de producto';
        $custom['index'] = 1;

        $data = [$this->productData($product, $custom)];
        return $this->layer($data, 'items', 'view_item');
    }

    public function productList($products, $custom = [], $callback = false, $extra = [])
    {
        $items = [];

        foreach ($products as $key => $product) {
            $item = $this->productData($product, $custom);
            $item['index'] = $key + 1;

            if (is_callable($callback)) {
                $item = array_merge($item, $callback($product));
            }

            $items[] = $item;
        }

        $data = $items;

        return $this->layer($data, 'items', 'view_item_list');
    }

    public function search($search_term, $products, $custom = [], $callback = false)
    {
        $items = [];

        foreach ($products as $key => $product) {
            $item = $this->productData($product, $custom);
            $item['index'] = $key + 1;

            if (is_callable($callback)) {
                $item = array_merge($item, $callback($product));
            }

            $items[] = $item;
        }

        $data = $items;
        return $this->layer($data, 'items', 'search', [
            'search_term' => $search_term,
        ]);
    }

    public function productData($product, $custom = [])
    {
        $item = [
            'item_id' => $product->id,
            'item_name' => $product->title,
            'price' => (float)number_format($product->realPrice($product->store_price, $product->tax_rate), 2, '.', ''),
            // 'price' => (float)str_replace(',', '.', $product->realPrice($product->store_price, $product->tax_rate))
        ];

        $item = array_merge($item, $custom);

        return $item;
    }

    public function orderData($order, $custom = [], $callback = false)
    {
        $data = [
            'affiliation' => Website::get('title'),
            'transaction_id' => (string)$order->id,
            'revenue' => (float)str_replace(',', '.', $order->total),
            'tax' => (float)str_replace(',', '.', $order->taxes),
            'currency' => 'EUR',
            'shipping' => (float)str_replace(',', '.', $order->realPrice($order->shipping_handling, $order->shipping_handling_tax_rate)),
            'products' => $this->orderItems($order, $callback),
        ];

        $data = array_merge($data, $custom);
        return $data;
    }

    public function orderItems($order, $callback = false)
    {
        $items = [];

        foreach ($order->line_items as $key => $item) {
            $_item = [
                'item_id' => (string)$item->product->id,
                'item_name' => $item->product->title,
                'quantity' => $item->quantity,
                'price' => number_format($item->realPrice($item->price, $item->tax_rate), 2, '.', ''),
                // 'price' => (string)str_replace(',', '.', $item->realPrice($item->price, $item->tax_rate))
            ];

            if (!empty($item->product_attribute)) {
                $_item['variant'] = $item->attributes_name;
            }

            if (is_callable($callback)) {
                $_item = array_merge($_item, $callback($item));
            }

            $items[] = $_item;
        }

        return $items;
    }


    public function addToCart($extra = [])
    {
        $items = [
            '"item_id"' => 'item.product.id',
            '"name"' => 'item.product.title',
            '"item_name"' => 'item.price_real',
            '"quantity"' => 'item.quantity',
        ];

        $items = array_merge($items, $extra);
        $_items = $this->arrayToScript($items);
        $out = <<<EOF
    <script>
    $('body').on( 'storeAddItem', function( event, item){
      var data = {
        "event": "add_to_cart",
        "ecommerce": {
            "items": [{
              $_items
            }]
        }
      };

      dataLayer.push(data)

    });
    </script>
EOF;

        return $out;
    }


    public function addToFavs()
    {
        $user_id = $this->Auth->user('id');

        if (!$user_id) {
            return;
        }

        $out = <<<EOF
    <script>
    $('body').on('storeAddedFav', function( event, data){
        console.log('add');
        dataLayer.push({ 
            "event": "AddToWhishList", 
            "userid": "$user_id"
        });
    });
    </script>
EOF;

        return $out;
    }


    public function login()
    {
        if ($user = Configure::read('AppEvents.login')) {
            $out = <<<EOF
        <script>
          var data = {
            "event": "Login",
            "userid": "{$user['id']}"
          };
    
          dataLayer.push(data)
        </script>
EOF;

            return $out;
        }
    }

    public function logout()
    {
        if ($user = Configure::read('AppEvents.logout')) {
            $out = <<<EOF
        <script>
          var data = {
            "event": "Logout",
            "userid": "{$user['id']}"
          };
    
          dataLayer.push(data)
        </script>
EOF;

            return $out;
        }
    }

    public function createAccount()
    {
        if ($user = Configure::read('AppEvents.register')) {
            \Cake\Log\Log::debug('Escribiendo register!!!!');
            $out = <<<EOF
        <script>
          var data = {
            "event": "CreateAccount",
            "userid": "{$user['id']}"
          };
    
          dataLayer.push(data)
        </script>
EOF;

            return $out;
        }
    }




    public function clickProduct($extra = [])
    {
        $items = [
            '"id"' => 'product.id',
            '"name"' => 'product.title',
        ];

        $items = array_merge($items, $extra);
        $_items = $this->arrayToScript($items);

        $section = $this->Nav->getSection();

        $out = <<<EOF
    <script>
    $("[gtm-link]").on( 'click', function(){
      var product = JSON.parse($(this).attr( 'gtm-link'));
      var list = product.list;
      var data = {
        "event": "select_item",
        "ecommerce": {
            "items": [{
              $_items
            }]
        }
      };
      dataLayer.push(data)
    });
    </script>
EOF;

        return $out;
    }

    public function removeFromCart($extra = [])
    {
        $items = [
            '"item_id"' => 'item.product.id',
            '"item_name"' => 'item.product.title',
            '"price"' => 'item.price_real',
            '"quantity"' => 'item.quantity',
        ];

        $items = array_merge($items, $extra);
        $_items = $this->arrayToScript($items);
        $list = $this->Nav->menuTitle('spa');
        $out = <<<EOF
    <script>
    $('body').on( 'storeDeleteItem', function( event, item){
      var data = {
        "event": "remove_from_cart",
        "ecommerce": {
            "items": [{
              $_items
            }]
        }
      };

      dataLayer.push(data)

    });
    </script>
EOF;

        return $out;
    }

    private function arrayToScript($data)
    {
        $out = [];

        foreach ($data as $key => $val) {
            $out[] = "$key: $val";
        }

        return implode(',', $out);
    }
}
