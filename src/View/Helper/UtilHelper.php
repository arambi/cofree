<?php
namespace Cofree\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\ORM\TableRegistry;
use Cofree\Lib\YoutubeUtil;

/**
 * Util helper
 */
class UtilHelper extends Helper
{

  public function youtubeId( $url)
  {
    return YoutubeUtil::youtubeId( $url);
  }

  public function youtubeImage( $url, $size = 'default')
  {
    return YoutubeUtil::youtubeImage( $url, $size);
  }

  public function youtubeEmbed( $url)
  {
    return YoutubeUtil::youtubeEmbed( $url);
  }
  
/**
 * Devuelve una URL de un registro que tiene el sistema de URLs definido en LinksConfigTrait
 * 
 * @param  Entity $content
 * @return string
 */
  public function externalUrl( $content)
  {
    $url = null;
    
    if( is_object( $content->settings) && $content->settings->link_type == 'section')
    {
      $url = TableRegistry::get( 'Section.Sections')->getUrl( $content->parent_id);
    }
    elseif( is_object( $content->settings) && isset( $content->settings->link_type) && $content->settings->link_type == 'url')
    {
      $url = $content->url;
    }
    elseif( is_object( $content->docs) && $content->settings->link_type == 'file')
    {
      $url = $content->docs->paths [0];
    }

    return $url;
  }

  public function paginatorSort( $key, $model)
  {
    return $this->_View->Paginator->sort( $key, '<i class="fa fa-caret-down'. 
            ($this->_View->Paginator->sortKey() == ($model .'.'. $key) && $this->_View->Paginator->sortDir() == 'desc' ? ' fa-caret-up' : '') 
          .'"></i>', ['escape' => false]);
  }

  public function googleMapStyles( $options = [])
  {
    $options = $options + [
      'stroke' => '#9c9c9c',
      'landscape' => '#f2f2f2',
      'landscapeFill' => '#ffffff',
      'road' => '#eeeeee',
      'roadText' => '#7b7b7b',
      'roadTextStroke' => '#ffffff',
      'water' => '#46bcec',
      'waterFill' => '#c8d7d4',
      'waterText' => '#070707',
      'waterTextStroke' => '#07fff0707',
    ];

    $content = <<<EOF
     [
          {
              "featureType": "all",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "weight": "2.00"
                  }
              ]
          },
          {
              "featureType": "all",
              "elementType": "geometry.stroke",
              "stylers": [
                  {
                      "color": "{$options ['stroke']}"
                  }
              ]
          },
          {
              "featureType": "all",
              "elementType": "labels.text",
              "stylers": [
                  {
                      "visibility": "on"
                  }
              ]
          },
          {
              "featureType": "landscape",
              "elementType": "all",
              "stylers": [
                  {
                      "color": "{$options ['landscape']}"
                  }
              ]
          },
          {
              "featureType": "landscape",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "color": "{$options ['landscapeFill']}"
                  }
              ]
          },
          {
              "featureType": "landscape.man_made",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "color": "{$options ['landscapeFill']}"
                  }
              ]
          },
          {
              "featureType": "poi",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "road",
              "elementType": "all",
              "stylers": [
                  {
                      "saturation": -100
                  },
                  {
                      "lightness": 45
                  }
              ]
          },
          {
              "featureType": "road",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "color": "{$options ['road']}"
                  }
              ]
          },
          {
              "featureType": "road",
              "elementType": "labels.text.fill",
              "stylers": [
                  {
                      "color": "{$options ['roadText']}"
                  }
              ]
          },
          {
              "featureType": "road",
              "elementType": "labels.text.stroke",
              "stylers": [
                  {
                      "color": "{$options ['roadTextStroke']}"
                  }
              ]
          },
          {
              "featureType": "road.highway",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "simplified"
                  }
              ]
          },
          {
              "featureType": "road.arterial",
              "elementType": "labels.icon",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "transit",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "water",
              "elementType": "all",
              "stylers": [
                  {
                      "color": "{$options ['water']}"
                  },
                  {
                      "visibility": "on"
                  }
              ]
          },
          {
              "featureType": "water",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "color": "{$options ['waterFill']}"
                  }
              ]
          },
          {
              "featureType": "water",
              "elementType": "labels.text.fill",
              "stylers": [
                  {
                      "color": "{$options ['waterText']}"
                  }
              ]
          },
          {
              "featureType": "water",
              "elementType": "labels.text.stroke",
              "stylers": [
                  {
                      "color": "{$options ['waterTextStroke']}"
                  }
              ]
          }
      ];
EOF;

    return $content;
  }

  public function linkify( $value, $protocols = array('http', 'mail'), array $attributes = array())
  {
      // Link attributes
      $attr = '';
      
      foreach ($attributes as $key => $val) 
      {
          $attr = ' ' . $key . '="' . htmlentities($val) . '"';
      }
      
      $links = array();
      
      // Extract existing links and tags
      $value = preg_replace_callback('~(<a .*?>.*?</a>|<.*?>)~i', function ($match) use (&$links) { return '<' . array_push($links, $match[1]) . '>'; }, $value);
      
      // Extract text links for each protocol
      foreach ((array)$protocols as $protocol) {
          switch ($protocol) {
              case 'http':
              case 'https':   $value = preg_replace_callback('~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { if ($match[1]) $protocol = $match[1]; $link = $match[2] ?: $match[3]; return '<' . array_push($links, "<a $attr href=\"$protocol://$link\">$link</a>") . '>'; }, $value); break;
              case 'mail':    $value = preg_replace_callback('~([^\s<]+?@[^\s<]+?\.[^\s<]+)(?<![\.,:])~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"mailto:{$match[1]}\">{$match[1]}</a>") . '>'; }, $value); break;
              case 'twitter': $value = preg_replace_callback('~(?<!\w)[@#](\w++)~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"https://twitter.com/" . ($match[0][0] == '@' ? '' : 'search/%23') . $match[1]  . "\">{$match[0]}</a>") . '>'; }, $value); break;
              default:        $value = preg_replace_callback('~' . preg_quote($protocol, '~') . '://([^\s<]+?)(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { return '<' . array_push($links, "<a $attr href=\"$protocol://{$match[1]}\">{$match[1]}</a>") . '>'; }, $value); break;
          }
      }
      
      // Insert all link
      return preg_replace_callback('/<(\d+)>/', function ($match) use (&$links) { return $links[$match[1] - 1]; }, $value);
  }

  public function groupArray( $contents, $number)
  {
    $groups = []; 
    $row = 0;
    
    foreach( $contents as $key => $content)
    {
      $groups [$row][] = $content;
  
      if( (($key + 1) % $number) == 0)
      {
        $row++;
      }
    }

    return $groups;
  }
  

}
