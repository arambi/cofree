<?php

namespace Cofree\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\Core\Configure;
use Website\Lib\Website;

/**
 * Socialnet helper
 */
class SocialnetHelper extends Helper
{
  

	private $__availables = [
		'facebook',
		'twitter',
		'google',
		'youtube',
		'vimeo',
		'linkedin',
		'instagram',
		'pinterest',
		'ivoox',
		'tripadvisor',
		'wikiloc',
		'blog',
		'issuu',
		'spotify',
	];

	public function display($options = [])
	{
		$options = $options + [
			'class' => 'fa fa-',
			'label' => false,
			'alias' => false,
      'icons' => []
		];

		$settings = (array)Website::get('settings');
		$out = [];

		$icons = [
			'blog' => 'wordpress'
		];



		foreach ($this->__availables as $name) {
			if (!empty($settings[$name])) {
				if (array_key_exists($name, $icons)) {
					$icon = $icons[$name];
				} else {
					$icon = $name;
				}

        if (isset($options['icons'][$icon])) {
          $icon_class = $options['class']. $options['icons'][$icon];
        }
				else if ($options['class'] == 'fa fa-' && $icon == 'youtube') {
					$icon_class = $options['class'] . $icon . '-play';
				} else {
					$icon_class = $options['class'] . $icon;
				}

				$icon = '<i class="' . $icon_class . '"></i>';

				if ($options['label']) {
					$label = $name;
					if (isset($options['alias'][$name])) {
						$label = $options['alias'][$name];
					}
					
					$icon .= '<span>' . ucfirst($label) . '</span>';
				}

				$out[] = '<li class="social-' . $name . '"><a itemprop="sameAs"  rel="nofollow" target="_blank" aria-label="' . $name . '" href="' . $settings[$name] . '">' . $icon . '</a></li>';
			}
		}

		return implode("\n", $out);
	}
}
