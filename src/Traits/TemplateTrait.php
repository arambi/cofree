<?php 

namespace Cofree\Traits;

use Cake\Core\Plugin;
use Cake\Core\Configure;

/**
 * Trait de métodos para la creación de ficheros
 */
trait TemplateTrait
{
/**
 * Añade el path del plugin para los templates
 * Necesario para que BakeTemplate pueda leer los template
 * 
 * @return void
 */
  private function __setPathTemplates( $plugin)
  {
    $paths = Configure::read('App.paths.templates');
    $paths [] = Plugin::path( $plugin) . 'src'. DS. 'Template' .DS;
    Configure::write('App.paths.templates', $paths);
  }

/**
 * Toma el contenido del fichero bootstrap.php de un plugin dado
 * 
 * @param  string $plugin
 * @return string
 */
  private function __getBootstrap( $plugin)
  {
    $path = Plugin::path( $plugin) . 'config' .DS. 'bootstrap.php';

    if( !file_exists( $path))
    {
      $content = "<?php \n";
      $this->createFile( $path, $content);
    }
    else
    {
      $content = file_get_contents( $path);
    }

    return $content;
  }

/**
 * Añade a un fichero el namespace de un fichero
 * Si este namespace esta ya añadido, no hace nada
 * Devuelve el contenido pasado
 * 
 * @param  string $contents  El contenido del fichero
 * @param  string $namespace El namespace
 * @return string 
 */
  private function __addUseNamespace( $contents, $namespace)
  {
    if( strpos( $contents, $namespace) === false)
    {
      $contents = str_replace( '<?php', "<?php \n". 'use '. $namespace .';' . "\n", $contents);
    }

    return $contents;
  }

/**
 * Inserta un código después de otro código
 * 
 * @param  string $content   El contenido del fichero
 * @param  string $reference Se insertará el código después del indicado aquí
 * @param  string $code      El código a insertar
 * @return string            
 */
  private function __insertAfter( $content, $reference, $code)
  {
    $pos = strpos( $content, $reference) + strlen( $reference);
    $content = substr_replace( $content,  $code, $pos, 0);
    return $content;
  }
}