<?php
namespace Cofree\Lib;

use Cake\Core\Plugin;
use Cake\Filesystem\Folder;
use Cake\ORM\TableRegistry;

class PluginUtils
{

  public static function getModels( $plugin)
  {
    $path = Plugin::path( $plugin);
    $table_path = $path .'src/Model/Table';

    $folder = new Folder( $table_path);
    $tree = $folder->tree( null, false, 'file');
    $tree = array_map( function( $value) use ($table_path) {
      $value = str_replace( $table_path . '/', '', $value);
      $value = str_replace( 'Table.php', '', $value);
      return $value;
    }, $tree);

    return $tree;
  }
}