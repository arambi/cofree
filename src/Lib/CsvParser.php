<?php 

namespace Cofree\Lib;

class CsvParser
{
  private $content;

  private $rowDelimiter = "\n";

  private $colDelimiter = "\t";

  private $keys = [];

  private $data = [];

  public function __construct( $content)
  {
    $this->content = $content;
  }

  public function data()
  {
    $rows = explode( $this->rowDelimiter, $this->content);

    foreach( $rows as $key => $row)
    {
      $dataRow = [];

      $cols = explode( $this->colDelimiter, $row);

      if( $key == 0)
      {
        foreach( $cols as $key2 => $value)
        {
          $this->keys [$key2] = $this->clean( $value);
        }
      }
      else
      {
        foreach( $cols as $key2 => $value)
        {
          $dataRow [$this->keys [$key2]] = $this->clean( $value);
        }

        $this->data [] = $dataRow;
      }

      
    }

    return $this->data;
  }

  private function clean( $value)
  {
    return trim( $value);
  }
}