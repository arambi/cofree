<?php
namespace Cofree\Lib;

use Cake\Network\Http\Client;
use Cake\Cache\Cache;
use Cake\Core\Configure;

/**
 * Utilidad para tomar la información de localización del usuario remoto a través de su IP
 * El objeto creado tiene las propiedades indicadas en $props
 *
 * Utiliza un servicio remoto para tomar los datos
 */

class RemoteLocation
{

/**
 * La url del servicio remoto
 * Puede ser cambiado y en ese caso cambiar el código en las funciones para la generación del objeto
 * @var string
 */
  private $serviceUrl = 'https://ipinfo.io/';

  private $mockIP = '83.173.153.127';

  const CONFIGURE_KEY = 'RemoteLocation';
/**
 * Constructor
 */
  public function __construct()
  {
    $data = $this->getData();

    if( $data && property_exists( $data, 'country'))
    {
      $this->country_code = $data->country;
      $this->city = $data->city;
    }
  }

/**
 * Toma los datos a través del servicio externo
 * @return Cake\Network\Http\Response
 */
  public function getData()
  {
    $http = new Client();
    $ip = env( 'REMOTE_ADDR');

    if( in_array( $ip, ['172.28.128.3', '172.28.128.1', '172.28.128.5', '127.0.0.1']) || $_SERVER ['SCRIPT_NAME'] == '/usr/local/bin/phpunit')
    {
      $ip = $this->mockIP;
    }

    $body = $this->loadCache( $ip);
    
    if( !$body)
    {
      try {
        $response = $http->get( $this->serviceUrl . $ip . '/json');
        $body = $response->body( 'json_decode');
        Cache::write( 'RemoteLocation.'. $ip, $body);
        $this->saveCache( $ip, $body);
      } catch (\Throwable $th) {
      }
    }

    // Países desactivados para la localización remota
    if( Configure::read( 'RemoteLocationDisable') && $body && is_object( $body) && in_array( $body->country, Configure::read( 'RemoteLocationDisable')))
    {
      return false;
    }

    return $body;
  }
  

  private function saveCache( $ip, $body)
  {
    Configure::write( self::CONFIGURE_KEY .'.'. $ip, $body);
  }

  private function loadCache( $ip)
  {
    $body = Configure::read( self::CONFIGURE_KEY .'.'. $ip);

    if( $body)
    {
      return $body;
    }

    return Cache::read( 'RemoteLocation.'. $ip);
  }

  

}