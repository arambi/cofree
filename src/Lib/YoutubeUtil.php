<?php 
namespace Cofree\Lib;

class YoutubeUtil
{
  public static function youtubeId( $url)
  {
    $id = false;
    $parts = parse_url( $url);

    if( !empty( $parts ['query']))
    {
      parse_str( $parts ['query'], $query);

      if( isset( $query ['v']))
      {
        $id = $query ['v'];
      }
    }
    else
    {
      $id = str_replace( '/', '', $parts ['path']);
    }

    return $id;
  }

  public static function youtubeImage( $url, $size = 'default')
  {
    if( $id = YoutubeUtil::youtubeId( $url))
    {
      return '//i.ytimg.com/vi/'. $id .'/'. $size .'.jpg';
    }
  }

  public static function youtubeEmbed( $url)
  {
    if( $id = YoutubeUtil::youtubeId( $url))
    {
      return 'https://www.youtube.com/embed/'. $id . '?rel=0';
    }
  }
  
}