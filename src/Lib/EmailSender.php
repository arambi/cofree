<?php
namespace Cofree\Lib;
/**
 * Se encarga de enviar mails
 * Es necesario implementar esta clase usando funciones de envio
 *
 */


use Cake\Network\Email\Email;
use Cake\Log\Log;
use Cake\Core\Configure;
use Website\Lib\Website;
use Cake\Event\EventManager;
use Cake\Event\Event;

class EmailSender
{
/**
 * Representa el nombre de la class que implementa a EmailSender
 * Es modificada por el método configure() de la class que lo implementa
 */
  public static $name;
  
  public static function getInstance()
  {
    $Email = new Email( 'default');
    $Email->emailFormat( 'html');
    $Email->from( Website::get( 'email'));
    // $Email->domain( Configure::read( 'Config.domain'));
    return $Email;
  }
  
  public static function config( Email $Email, $key = false)
  {
    if( $key === false)
    {
      $key = 'notify';
    }
    
    $config = Configure::read( 'Email.'. $key);
    $Email->from( Website::get( 'email'));
    return $Email;
  }
  
  public static function changeLanguage( $lang)
  {
    Configure::write( 'Config.language', $lang);
  }
  
  public static function sendMail( Email $Email)
  {
    try {
      return $Email->send();
    } catch (Exception $e) {
      $template = $Email->template();
      $to = $Email->to();
      $from = $Email->from();
      $result = sprintf( "Error al enviar un mail a %s.\n
      De: %s\n
      Tema: %s\n
      Template: %s\n
      Error: %s\n
      Servidor: %s", 
      current( $to),
      current( $from),
      $Email->subject(),
      $template ['template'] .' ('. $template ['layout'] .')',
      $e->getMessage(),
      gethostname());
      Log::write( $result, LOG_DEBUG);
    }
  }
  
  public static function send( $method)
  { 
    $args	= func_get_args();
    $method = $args [0];
    $email = self::getInstance();
    unset( $args [0]);
    $args [0] = $email;
    ksort( $args);
    call_user_func_array( array( get_called_class(), $method), $args);
  
    // Dispatch event 
    $class = new \ReflectionClass( get_called_class());
    $event = new Event( 'Email.'. $class->getShortName() .'.'. $method, $email);
    EventManager::instance()->dispatch( $event);
    
    if( Configure::read( 'debug') > 0)
    {
      $result = self::sendMail( $email);
    }
    else
    {
      return self::sendMail( $email);
    }
  }
  


  
}