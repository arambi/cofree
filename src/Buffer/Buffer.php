<?php

namespace Cofree\Buffer;

use Cake\Core\Configure;

class Buffer
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected static $_defaultConfig = [];

    private static $__script = 'Buffer';
    private static $__top = 'Buffer.top';

    private static $key = false;

    public static function scriptStart()
    {
        self::start();
    }

    public static function scriptEnd()
    {
        self::end();
    }

    public static function writeScript()
    {
        self::write();
    }

    public static function startTop()
    {
        ob_start();
    }

    public static function endTop()
    {
        $buffer = ob_get_clean();

        if (!Configure::read(self::$__top)) {
            Configure::write(self::$__top, []);
        }

        $total = Configure::read(self::$__top);
        $total[] = $buffer;
        Configure::write(self::$__top, $total);
    }


    public static function start($key = false)
    {
        self::$key = $key;
        ob_start();
    }

    public static function end()
    {
        $buffer = ob_get_clean();

        $key = self::$key ? self::$key : 'script';

        if (!Configure::read(self::$__script . '.' . $key)) {
            Configure::write(self::$__script . '.' . $key, []);
        }

        $total = Configure::read(self::$__script . '.' . $key);
        $total[] = $buffer;
        Configure::write(self::$__script . '.' . $key, $total);
    }

    public static function write($key = false)
    {
        if (!$key) {
            $key = 'script';
        }

        $scripts = Configure::read(self::$__script . '.' . $key);

        if (!$scripts) {
            return '';
        }

        return implode("\n", $scripts);
    }

    public static function writeTop()
    {
        $html = Configure::read(self::$__top);
        if (!$html) {
            return '';
        }

        return implode("\n", $html);
    }
}
