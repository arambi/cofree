<?php
namespace Cofree\Controller;

use Cofree\Controller\AppController;
use Cake\Utility\Inflector;

/**
 * Templates Controller
 *
 * @property \Cofree\Model\Table\TemplatesTable $Templates
 */
class TemplatesController extends AppController
{
  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');

    if( isset( $this->Auth))
    {
      $this->Auth->allow();
    }
  }

  public function index()
  {
    $this->viewBuilder()->layout( 'ajax');
    $path = Inflector::camelize( $this->request->param( 'ctrl')) .'/'. $this->request->param( 'file');
    $this->render( $path);
  }
}
