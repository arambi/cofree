<?php

namespace Cofree\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use ReCaptcha\ReCaptcha;

/**
 * Recaptcha component
 */
class RecaptchaComponent extends Component
{

    public $sessionKey = 'ValidRecaptcha';

    /**
     * Default configuration.
     *
     * @var array
     */
    public function initialize(array $config)
    {
        // $this->_registry->getController()->viewBuilder()->helpers( ['Cofree.Recaptcha']);
    }

    public function verify($entity = null)
    {
        $request = $this->getController()->getRequest();
        $recaptcha = new ReCaptcha(Configure::read('Recaptcha.secret'));
        $resp = $recaptcha->verify($request->getData('g-recaptcha-response'), env('REMOTE_ADDR'));

        if (!$resp->isSuccess()) {
            $this->_registry->getController()->set('recaptchaError', __d('app', 'La validación no es correcta'));

            if ($entity) {
                $entity->errors('recaptcha', [__d('app', 'La validación no es correcta')]);
            }

            return false;
        } else {
            $request->getSession()->write($this->sessionKey, true);
            return true;
        }
    }
}
