<?php
namespace Cofree\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Event\Event;

/**
 * LawCookies component
 */
class LawCookiesComponent extends Component
{
  public $components = ['Cookie'];

  public function beforeFilter( Event $event)
  {
    $this->Cookie->configKey( 'cookie_control', [
        'expires' => '+30 days',
        'httpOnly' => true
    ]);

    $this->__cookieControl();
  }

  private function __cookieControl()
  {
    Configure::write( 'LawCookies.cookie_control', 0);
    
    if( !$this->Cookie->check( 'cookie_control'))
    {
      $this->Cookie->write( 'cookie_control', '2', true);
      Configure::write( 'LawCookies.cookie_control', 2);
    }
  }
}
