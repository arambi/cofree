<?php
namespace Cofree\Shell;

use Cake\Console\Shell;
use Cake\Core\Plugin;
use Cake\Console\ConsoleOptionParser;
use Cake\Cache\Cache;

/**
 * Git shell command.
 */
class GitShell extends Shell
{

  public function initialize()
  {
    parent::initialize();
    $this->parser = parent::getOptionParser();
  }

/**
 * Añade ficheros para el git de la rama
 */
  public function add()
  {
    $this->chdir();
    $file = $this->args [1];

    if( $file == 'all')
    {
      $file = '*';
    }

    exec( 'git add '. $file, $out);
    $this->out( $out);
  }

/**
 * Hace un commit para la rama
 */
  public function commit()
  {
    $this->chdir();
    $message = $this->args [1];
    exec( 'git commit -a -m "'. $message .'"', $out);
    $this->out( $out);
  }

  public function pull()
  {
    exec( 'git pull', $out);
    $this->out( $out);
    Cache::clear();
  }

/**
 * Hace un push para la rama
 */
  public function push()
  {
    $this->chdir();
    $branch = isset( $this->args [1]) ? $this->args [1] : 'master';
    exec( 'git push -u origin '. $branch, $out);
    $this->out( $out);
  }


/**
 * Da todos los status para todos los plugins de la aplicación
 */
  public function statuses()
  {
    $plugins = Plugin::loaded();
    
    $plugins_changed = [];

    foreach( $plugins as $plugin)
    {
      if( $this->chdir( $plugin))
      {
        $out = null;
        exec( 'git status', $out);

        if( strpos( implode( " ", $out), 'Changes') !== false)
        {
          $this->out();
          $this->out();
          $this->out();
          $this->out( '============ '. $plugin .' ============');

          $this->out( $out);

          $plugins_changed [] = $plugin;
        }

        unset( $out);
      }
    }

    if( !empty( $plugins_changed))
    {
      $this->out( '============== Plugins con cambios ==============');
      $this->out( implode( "\n", $plugins_changed));
    }
    else
    {
      $this->out( 'Todo esta up-date');
    }
  }

/**
 * Cambia al directorio del plugin
 * Si no se da el argumente en el método, tomará el primer argumento del shell
 * 
 * @param  string|null $plugin
 * @return boolean
 */
  private function chdir( $plugin = null)
  {
    if( $plugin === null)
    {
      $plugin = $this->args [0];
    }

    $plugin = ucfirst( $plugin);
    $dir = Plugin::path( $plugin);

    if( !is_dir( $dir .DS. '.git'))
    {
      return false;
    }

    chdir( $dir);
    return true;
  }

/**
 * Parser por defecto para varios de los subcomandos
 * 
 * @return ConsoleOptionParser
 */
  private function defaultParser()
  {
    $parser = new ConsoleOptionParser();
    $parser
      ->addArgument( 'plugin', [
          'help' => 'El nombre del plugin',
          'required' => true,
      ]);

    return $parser;
  }



  public function getOptionParser()
  {
    $parser = $this->parser;
    $parser->description(
        'Utilidades GIT para commits y push de los plugins'
    ) 
      ->addSubcommand( 'add', [
          'help' => 'Añade ficheros.',
          'parser' => $this->defaultParser()->addArgument( 'file', [
            'help' => 'El fichero a añadir o all para asterisco (*)',
            'required' => true,
          ])
      ])
      ->addSubcommand( 'statuses', [
          'help' => 'Retorna los status de todos los plugins',
      ])
      ->addSubcommand( 'commit', [
          'help' => 'Hace un commit para el plugin dado.',
          'parser' => $this->defaultParser()->addArgument( 'message', [
            'help' => 'El mensaje',
            'required' => true,
          ])
      ])
      ->addSubcommand( 'push', [
          'help' => 'Hace un commit para el plugin dado.',
          'parser' => $this->defaultParser()->addArgument( 'branch', [
            'help' => 'El branch (por defecto es master)',
          ])
      ])
      ->addSubcommand( 'pull', [
          'help' => 'Hace un pull y borra la cache.',
      ])
     ;
    return $parser;
  }




}
