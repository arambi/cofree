<?php

/**
 * CofreeWeb (copyleft) (http://cofreeweb.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyleft notice.
 *
 * @copyleft      CofreeWeb (copyleft) (http://cofreeweb.com)
 * @link          http://cofreeweb.com
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace Cofree\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\Core\Plugin;

class CofreeShell extends Shell
{
  /**
   * Inserta en la base de datos valores iniciales por defecto que necesita la aplicación
   * @return void
   */
  public function setinitialdata()
  {
    $this->out('Insertando datos para la tabla groups...');
    $groups = TableRegistry::get('User.Groups');
    $groupdata = [
      'name' => 'Administrador',
      'permissions' => '["master"]',
      'created' => date('Y-m-d H:i:s'),
      'modified' => date('Y-m-d H:i:s'),
    ];
    $entity = $groups->newEntity($groupdata);
    $groups->save($entity);

    $this->out('Insertando datos para la tabla users...');
    $users = TableRegistry::get('User.Users');
    $userdata = [
      'group_id' => '1',
      'username' => 'cofreedevs',
      'name' => 'Cofreeweb Developers',
      'password' => 'devs',
      'password2' => 'devs',
      'email' => 'devs@cofreeweb.com',
      'status' => 'active',
      'created' => date('Y-m-d H:i:s'),
      'modified' => date('Y-m-d H:i:s'),
    ];
    $entity = $users->newEntity($userdata);
    $users->save($entity);

    $this->out('Insertando datos para la tabla languages...');
    $langs = TableRegistry::get('I18n.Languages');
    $langdata = [
      'name' => 'Español',
      'iso2' => 'es',
      'iso3' => 'spa',
      'locale' => 'es_ES',
      'by_default' => '1',
      'created' => date('Y-m-d H:i:s'),
      'modified' => date('Y-m-d H:i:s'),
    ];
    $entity = $langs->newEntity($langdata);
    $langs->save($entity);
  }

  /**
   * Actualiza los plugins de cofree instalados en el proyecto que esten en bitbucket
   * @return void
   */
  public function updateplugins()
  {
    $bitbucket_repos = shell_exec('curl --user cofreeweb:20cafe13 https://api.bitbucket.org/1.0/user/repositories');;
    $br = json_decode($bitbucket_repos, true);

    $repositories = array();
    foreach ($br as $key => $r) {
      $repositories[$r['name']] = $r['name'];
    }

    $localrepositories = array();
    exec('ls plugins/', $localrepositories);

    foreach ($localrepositories as $key => $l) {
      if (array_key_exists($l, $repositories)) {
        $this->out("Actualizando " . $l);
        shell_exec('cd plugins/' . $l);
        $output = shell_exec('git pull');
        echo $output;
        $this->out();
        shell_exec('cd ../../');
      }
    }
  }


  public function status()
  {
    $plugins = Plugin::loaded();

    $plugins_changed = [];

    foreach ($plugins as $plugin) {
      $dir = Plugin::path($plugin);

      if (is_dir($dir . DS . '.git')) {
        chdir($dir);
        $out = null;
        exec('git status', $out);

        if (strpos(implode(" ", $out), 'Changes') !== false) {
          $this->out();
          $this->out();
          $this->out();
          $this->out('============ ' . $plugin . ' ============');

          $this->out($out);

          $plugins_changed[] = $plugin;
        }

        unset($out);
      }
    }

    if (!empty($plugins_changed)) {
      $this->out('============== Plugins con cambios ==============');
      $this->out(implode("\n", $plugins_changed));
    } else {
      $this->out('Todo esta up-date');
    }
  }

  public function reslug()
  {
    $model = $this->in('Indica un model (plugin.model)');
    $field = $this->in('Indica el campo');
    $table = $this->getTableLocator()->get($model);

    foreach ($table->find() as $entity) {
      if (empty($entity->$field)) {
        $table->save($entity);
      }
    }
  }
}
