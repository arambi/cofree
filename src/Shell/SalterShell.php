<?php
namespace Cofree\Shell;

use Cake\Console\Shell;

/**
 * Salter shell command.
 */
class SalterShell extends Shell
{

  /**
   * Manage the available sub-commands along with their arguments and help
   *
   * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
   *
   * @return \Cake\Console\ConsoleOptionParser
   */
  public function getOptionParser()
  {
      $parser = parent::getOptionParser();

      return $parser;
  }

  /**
   * main() method.
   *
   * @return bool|int|null Success or error code.
   */
  public function main()
  {
    $model = $this->in( 'Indica un model');
    $table = $this->loadModel( $model);

    $limit = 50;
    $offset = 0;

    while( true)
    {
      $contents = $table->find()
        ->limit( $limit)
        ->offset( $offset)
        ->order( $table->alias() .'.id')
        ->all();
      
      if( $contents->count() == 0)
      {
        break;
      }
      
      foreach( $contents as $content)
      {
        $salt = $table->generateSalt();
        $query = $table->query();
        $query->update()
          ->set(['salt' => $salt])
          ->where(['id' => $content->id])
          ->execute();
      }

      $offset = $offset + $limit;
    }     
  }
}
