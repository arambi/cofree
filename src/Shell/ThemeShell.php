<?php
namespace Cofree\Shell;

use Cake\Console\Shell;
use Cake\Core\Plugin;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Core\App;
use Cake\Utility\Inflector;

/**
 * Creación de themes
 */
class ThemeShell extends Shell
{
/**
 * Conjunto de templates a copiar
 * @var array
 */
  private $templates = [];

/**
 * El path del plugin o theme
 * @var string
 */
  private $pluginPath;

/**
 * Instala un theme, copiando los ficheros señalados en cada plugin, en el fichero templates.json, que se encuentra en la raiz del plugin
 *
 * @example bin/cake Cofree.theme install
 * @return void
 */
  public function install()
  {
    $name = $this->in( "Indica el nombre del theme");

    $this->dispatchShell( 'bake', 'plugin', $name);

    $this->pluginPath = ROOT .DS. 'plugins' .DS. Inflector::camelize( $name) .DS;

    $plugins = Plugin::loaded();

    foreach( $plugins as $plugin)
    {
      if( $this->__hasTemplates( $plugin))
      {
        $path = Plugin::path( $plugin) .'templates.json';
        $json = file_get_contents( $path);
        $config = json_decode( $json, true);
        
        foreach( $config ['templates'] as $template)
        {
          if( substr( $template, -1) == '*')
          {
            $this->__getRecursive( $template, $plugin);
          }
          else
          {
            $this->__getOne( $template, $plugin);
          }
        }
      }
    }

    $this->__writeFiles();
  }

  private function __writeFiles()
  {
    $Folder = new Folder();

    foreach( $this->templates as $plugin => $templates)
    {
      foreach( $templates as $template)
      {
        $destiny = str_replace( Plugin::path( $plugin), $this->pluginPath, $template);
        $Folder->create( dirname( $destiny));
        $this->out( vsprintf( 'Copiando fichero %s en %s', [$template, $destiny]));
        copy( $template, $destiny);
      }
    }
  }

/**
 * Añade a los templates un fichero dado
 * 
 * @param  string $path  El path al fichero
 * @param  string $plugin 
 * @return void        
 */
  private function __getOne( $path, $plugin)
  {
    $path = Plugin::path( $plugin) . 'src' .DS. 'Template' .DS. $path;

    if( file_exists( $path))
    {
      $this->__addTemplates( $path, $plugin);
    }
  }

/**
 * Añade todos los ficheros json y ctp de un path dado dentro de un plugin dado
 * 
 * @param  string $path  
 * @param  string $plugin
 * @return void         
 */
  private function __getRecursive( $path, $plugin)
  {
    $path = str_replace( '/*', '', $path);
    $path = Plugin::path( $plugin) . 'src' .DS. 'Template' .DS. $path;
    $Folder = new Folder( $path);
    $files = $Folder->findRecursive( '.*(ctp|json)');
    
    if( !empty( $files))
    {
      $this->__addTemplates( $files, $plugin);
    }
  }

/**
 * Añade los ficheros a la propiedad $this->templates [plugins]   
 * @param  string|array $templates [description]
 * @param  string $plugin
 * @return void        
 */
  private function __addTemplates( $templates, $plugin)
  {
    if( !is_array( $templates))
    {
      $templates = array( $templates);
    }

    if( !isset( $this->templates [$plugin]))
    {
      $this->templates [$plugin] = [];
    }

    $this->templates [$plugin] = array_merge( $this->templates [$plugin], $templates);
  }
 
/**
 * Busca en un plugin dado si existe el fichero templates.json
 * 
 * @param  string $plugin
 * @return boolean
 */
  private function __hasTemplates( $plugin)
  {
    $path = Plugin::path( $plugin) .'templates.json';
    return file_exists( $path);
  }
}
