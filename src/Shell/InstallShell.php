<?php
/**
 * CofreeWeb (copyleft) (http://cofreeweb.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyleft notice.
 *
 * @copyleft      CofreeWeb (copyleft) (http://cofreeweb.com)
 * @link          http://cofreeweb.com
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace Cofree\Shell;

use Cake\Console\Shell;
use Cake\Core\Plugin;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;

/**
 * Realiza la instalación de la aplicación
 */
class InstallShell extends Shell
{
  public $tasks = [
    'Bake.BakeTemplate',
  ];

/**
 * Ejecuta diversas acciones que serán a su vez ejecutadas en cada plugin
 * 
 * @return void
 */
  public function main()
  {
    $this->copyAppFiles();

    // Creación de ficheros de configuración
    $this->configurator();

    // Creación de tablas en las bases de datos
    $this->migrate();

    // Creación de datos en las bases de datos
    $this->dump();

    
    $this->createPlugin();
  }

  public function restore()
  {
    $this->rollback();
    $this->migrate();
    $this->dump();
  }


  public function createPlugin()
  { 
    $plugin = $this->in( 'Indica el nombre del plugin');
    $pluginPath = ROOT .DS. 'plugins' .DS. ucfirst( $plugin);
    $this->dispatchShell( 'bake plugin '. $plugin);
    $this->createFile( $pluginPath .DS. 'config' .DS. 'bootstrap.php', "<?php \n");
    Plugin::load( ucfirst( $plugin), ['bootstrap' => true, 'routes' => true, 'autoload' => true]);
    $this->copyPluginTemplates( ucfirst( $plugin));

    // Pone el Theme en el Site
    $site = TableRegistry::get( 'Website.Sites')->find()->first();
    $site->set( 'theme', ucfirst( $plugin));
    TableRegistry::get( 'Website.Sites')->save( $site);
  }

  private function copyPluginTemplates( $plugin)
  {
    $this->__setPathTemplates();

    // Copia el logo por defecto
    $this->cmd( 'cp '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/img/* '. ROOT .'/webroot/img/');

    // Copia los favicons
    $this->cmd( 'cp '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/icons/* '. ROOT .'/webroot/');

    // Copia los css
    $this->cmd( 'cp '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/css/* '. Plugin::path( $plugin) .'webroot/css/');
    
    $elements = [
      'bottom',
      'footer',
      'head',
      'header',
      'top',
    ];

    foreach( $elements as $el)
    {
      $this->BakeTemplate->set([
        'plugin' => strtolower( $plugin),
      ]);

      $contents = $this->BakeTemplate->generate( 'Shells/layout/'. $el);
      $this->createFile( Plugin::path( $plugin) .'src/Template/Element/layout/'. $el .'.ctp', $contents);
    }

    // Copia diversos CTP
    $this->cmd( 'mkdir '. Plugin::path( $plugin) .'src/Template/Layout/');
    $this->cmd( 'cp '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/default.ctp '. Plugin::path( $plugin) .'src/Template/Layout/');
    $this->cmd( 'cp '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/default.json '. Plugin::path( $plugin) .'src/Template/Layout/');
    $this->cmd( 'cp '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/pagination.ctp '. Plugin::path( $plugin) .'src/Template/Element/');
    $this->cmd( 'cp '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/section_title.ctp '. Plugin::path( $plugin) .'src/Template/Element/');

    // Copia los Cell
    $this->cmd( 'cp -R '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/Cell '. Plugin::path( $plugin) .'src/Template/');

    // Páginas de error
    $this->BakeTemplate->set([
      'plugin' => $plugin,
    ]);

    $contents = $this->BakeTemplate->generate( 'Shells/Error/error');
    $filepath = ROOT .'/src/Template/Layout/error.ctp';
    $this->createFile( $filepath, $contents);

    $this->cmd( 'cp -f '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/Error/error400.ctp '. ROOT .'/src/Template/Error/');
    $this->cmd( 'cp -f '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/Error/error500.ctp '. ROOT .'/src/Template/Error/');

    // Cookies
    $this->cmd( 'cp -f '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/cookies.ctp '. ROOT .'/src/Template/Element/');
    
    $this->webrootTemplates();

    $this->dispatchShell( 'plugin assets symlink');
  }

  public function copyAppFiles()
  {
    // Copia el ajax.ctp
    $this->cmd( 'cp '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/ajax.ctp '. APP .'Template/Layout/');

    // Copia el Application.php
    $this->cmd( 'cp '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/Application.php '. APP);
    
     // Copia el package y gulp
    $this->cmd( 'cp '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/package.json '. ROOT .'/');
    $this->cmd( 'cp '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/gulpfile.babel.js '. ROOT .'/');
    $this->cmd( 'cp '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/.babelrc '. ROOT .'/');
  }

  public function webrootTemplates()
  {
    // Crea el directorio img y css
    new Folder( ROOT .'/webroot/img', true);
    new Folder( ROOT .'/webroot/css', true);
    $this->cmd( 'cp -R '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/css '. ROOT .'/webroot/');
    $this->cmd( 'cp -R '. Plugin::path( 'Cofree') .'src/Template/Bake/Shells/robots.txt '. ROOT .'/webroot/');
  }

  public function cmd( $cmd)
  {
    $this->out( $cmd);
    system( $cmd);
  }

  private function __setPathTemplates()
  {
    $paths = Configure::read('App.paths.templates');
    $paths [] = Plugin::path( 'Cofree') . 'src'. DS. 'Template' .DS;
    Configure::write('App.paths.templates', $paths);
  }

/**
 * Se encarga de ejectuar las migraciones por cada plugin
 * 
 * @return void
 */
  public function migrate()
  {
    $plugins = Plugin::loaded();

    unset( $plugins [array_search( 'Cofree', $plugins)]);
    unset( $plugins [array_search( 'Website', $plugins)]);
    array_unshift( $plugins, 'Website');
    array_unshift( $plugins, 'Cofree');

    foreach( $plugins as $plugin)
    {
      if( $this->__hasMigrations( $plugin))
      {
        $this->out( 'Migrations para el plugin '. $plugin);
        exec( 'bin/cake migrations migrate --plugin '. $plugin);
      }
    }

    exec( 'rm tmp/cache/models/*');
  }

  public static function migrations()
  {
    exec( 'bin/cake install migrate');
    exec( 'rm tmp/cache/models/*');
  }

  /**
 * Se encarga de ejectuar las migraciones para atrás por cada plugin
 * 
 * @return void
 */
  public function rollback()
  {
    $plugins = Plugin::loaded();

    foreach( $plugins as $plugin)
    {
      if( $this->__hasMigrations( $plugin))
      {
        $this->out( 'Migrations rollback para el plugin '. $plugin);
        exec( 'bin/cake Migrations.migrations rollback --plugin '. $plugin);
      }
    }
  }

/**
 * Se encarga de lanzar el comando Plugin.dumper 
 * En cada plugin podría haber un Shell Dumper donde desde el método main() se hacen operaciones de insertado de datos en la base de datos
 *   
 * @return void
 */
  public function dump()
  {
    $plugins = Plugin::loaded();

    // Aquí indicamos el orden de los plugins para realizar el dump
    // Es importante porque los que se señalan abajo dependen de que se haya realizado anteriormente el dump en los anteriores
    $dependents = [
      'User',
      'I18n',
      'Website',
      'Section'
    ];

    $plugins = array_diff( Plugin::loaded(), $dependents);
    $plugins = $dependents + $plugins;

    foreach( $plugins as $plugin)
    {
      if( $this->__hasPluginShell( 'Dumper', $plugin))
      {
        $this->out( 'Dumper para el plugin '. $plugin);
        $this->dispatchShell( $plugin .'.dumper');
      }
    }    
  }

/**
 * Se encarga de lanzar el comando Plugin.configurator 
 * En cada plugin podría haber un Shell Configurator donde desde el método main() se hacen operaciones de creación de ficheros de configuración
 *   
 * @return void
 */
  public function configurator()
  {
    $plugins = Plugin::loaded();

    foreach( $plugins as $plugin)
    {
      if( $this->__hasPluginShell( 'Configurator', $plugin))
      {
        $this->out( 'Configurator para el plugin '. $plugin);
        $this->dispatchShell( $plugin .'.configurator');
      }
    }    
  }

/**
 * Comprueba si el Shell Dumper se encuentra en el plugin dado
 * 
 * @param  string $plugin
 * @return boolean
 */
  private function __hasPluginShell( $shell, $plugin)
  { 
    $path = Plugin::path( $plugin);
    return file_exists( $path . 'src' .DS. 'Shell' .DS. $shell .'Shell.php');
  }


  private function __hasMigrations( $plugin)
  {
    $path = Plugin::path( $plugin) .'config' .DS. 'Migrations';

    $Folder = new Folder( $path);
    $files = $Folder->read( true, true);
    return !empty( $files [1]);
  }
}
