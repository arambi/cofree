<?php
namespace Cofree\Shell;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;

/**
 * Encodes shell command.
 */
class EncodesShell extends Shell
{
  /**
   * Manage the available sub-commands along with their arguments and help
   *
   * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
   *
   * @return \Cake\Console\ConsoleOptionParser
   */
  public function getOptionParser()
  {
      $parser = parent::getOptionParser();

      return $parser;
  }

  /**
   * main() method.
   *
   * @return bool|int|null Success or error code.
   */
  public function main()
  {
  }

  public function mb4()
  {
    $connection = ConnectionManager::get('default');
    $schemas = $connection->getSchemaCollection();
    $tables = $schemas->listTables();
    _d( $tables);
  }
}
