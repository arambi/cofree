<?php

namespace Cofree\Shell\Task;

use Cake\Console\Shell;
use Cake\Core\Plugin;
use Cake\Filesystem\File;
use Cake\Error\Debugger;
use Cake\Utility\Text;

/**
 * Utilidades de ayuda para los Shells de instalación de los plugins (Dumpers, Configurations...)
 */
class InstallerTask extends Shell
{

/**
 * Implementación de Shell::out()
 * Añade la class y el método desde donde se hace el out del mensaje
 *   
 * @param string|array|null $message A string or an array of strings to output
 * @param int $newlines Number of newlines to append
 * @param int $level The message's output level, see above.
 * @return int|bool Returns the number of bytes returned from writing to stdout.
 */
  public function out( $message = null, $newlines = 1, $level = Shell::NORMAL)
  {
    $trace = Debugger::trace([
      'format' => 'array',
      'depth' => 3
    ]);
    
    parent::out( $trace [2]['class'] .'::'. $trace [2]['function'] . '() - '. $message);
  }

/**
 * Toma los datos de un fichero JSON y devuelve el array
 * Los ficheros deben estar en la ruta Plugin/src/Shell/Data
 * 
 * @param  string $plugin
 * @param  string $file
 * @return array
 */
  public function getData( $plugin, $file, $replacements = [])
  {
    $path = Plugin::path( $plugin) . 'src' .DS. 'Shell' .DS. 'Data' .DS;
    $content = file_get_contents( $path . $file .'.json');
    $content = Text::insert( $content, $replacements);
    return json_decode( $content, true);
  }
}