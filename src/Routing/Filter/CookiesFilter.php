<?php

namespace Cofree\Routing\Filter;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\Event\EventManagerTrait;
use Cake\Routing\DispatcherFilter;
use Cake\Routing\Router;
use Cake\View\View;

class CookiesFilter extends DispatcherFilter
{


  public function beforeDispatch(Event $event)
  {

  }

  public function afterDispatch(Event $event)
  {
    if( $event->data ['request']->param( 'prefix') != 'admin' && Configure::read( 'LawCookies.cookie_control') == 2)
    {
      $view = new View( $event->data ['request'], $event->data ['response']);
      $element = $view->element( 'cookies');
      $response = $event->data ['response'];
      $body = $response->body();
      $pos = strpos( $body, '</body>');
      
      if( $pos === false) 
      {
        return;
      }
      $body = str_replace( '</body>', $element . '</body>', $body);

      $script = '<script type="text/javascript">
          $(function(){
            $(".cookies-close").click(function(){
              $(".cookies").remove();
              return false;
            })
          })
        </script>
      ';

      $pos = strpos( $body, '</html>');

      $body = str_replace( '</html>', $script . '</html>', $body);
      $response->body($body);
    }
  }

}
