<?php
namespace Cofree\Middleware;

use Cake\Core\Configure;
use Cake\Core\InstanceConfigTrait;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\Utility\Inflector;
use Cake\View\View;

class CookiesBMiddleware 
{
	use InstanceConfigTrait;

	/**
	 * @var array
	 */
	protected $_defaultConfig = [
		'when' => null,
	];

	/**
	 * @param array $config
	 */
	public function __construct(array $config = []) {
		$this->config($config);
	}

	/**
	 * Checks if a requested cache file exists and sends it to the browser.
	 *
	 * @param \Psr\Http\Message\ServerRequestInterface $request The request.
	 * @param \Psr\Http\Message\ResponseInterface $response The response.
	 * @param callable $next The next middleware to call.
	 * @return \Psr\Http\Message\ResponseInterface A response.
	 */
	public function __invoke(Request $request, Response $response, $next) 
  {
    $_response = $next($request, $response);
		$this->setScript( $request, $_response);
		return $_response;
		return $next($request, $response);
	}

	private function setScript( $request, $response)
	{
		$cookie = $request->getCookie( 'cookie_control');

		if( $request->param( 'prefix') != 'admin' && !$cookie)
    {
      $view = new View( $request, $response);
      $element = $view->element( 'cookies');
      $body = $response->body();
      $pos = strpos( $body, '</body>');
      
      if( $pos === false) 
      {
        return;
      }
      $body = str_replace( '</body>', $element . '</body>', $body);

      $script = '<script type="text/javascript">
          $(function(){
            $("#c-cookies__accept").click(function(){
              $(".c-cookies").remove();
              Cookies.set("cookie_control", 2, { expires: 5184000 });
              return false;
            })
          })
        </script>
      ';

      $pos = strpos( $body, '</html>');

      $body = str_replace( '</html>', $script . '</html>', $body);
      $response->body($body);
			// setcookie("cookie_control", 1, time()+( 60 * 60 * 24 * 30));
		}
	}
}
