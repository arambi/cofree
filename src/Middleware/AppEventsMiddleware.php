<?php
namespace Cofree\Middleware;

use Cake\Core\Configure;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * AppEvents middleware
 */
class AppEventsMiddleware
{
    /**
     * Invoke method.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request.
     * @param \Psr\Http\Message\ResponseInterface $response The response.
     * @param callable $next Callback to invoke the next middleware.
     * @return \Psr\Http\Message\ResponseInterface A response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        if (method_exists($request, 'getSession')) {
            $session = $request->getSession();
            
            if ($session->read('AppEvents')) {
                Configure::write('AppEvents', $session->read('AppEvents'));
                $session->delete('AppEvents');
            }
        }

        return $next($request, $response);
    }
}
