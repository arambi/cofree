<?php
namespace Cofree\Pdf\Engine;

use CakePdf\Pdf\Engine\AbstractPdfEngine;

class MpdfEngine extends AbstractPdfEngine
{

    /**
     * Generates Pdf from html
     *
     * @return string raw pdf data
     */
    public function output()
    {
        //mPDF often produces a whole bunch of errors, although there is a pdf created when debug = 0
        //Configure::write('debug', 0);
        $MPDF = new \mPDF($this->getConfig( 'options'));
        $MPDF->writeHTML($this->_Pdf->html());
        return $MPDF->Output('', 'S');
    }
}
